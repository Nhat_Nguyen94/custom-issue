	<!DOCTYPE html>
	<html lang="{{ Cookie::get('laravel_language','vi') }}">
	<head>
		<title>{!!  trans('content.gotit_title')  !!}</title>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	    <meta http-equiv="Pragma" content="no-cache" />
	    <meta http-equiv="Expires" content="0" />
	    <meta name="csrf-token" content="{!!  csrf_token()  !!}" />
	    <meta http-equiv="content-language" content="{!!  Cookie::get('laravel_language','vi')  !!}">
	    <meta property="fb:app_id" content="460418860782124" />
	    @if($voucher)
	 
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
	    <meta name="format-detection" content="telephone=no">
	    <meta name="thumbnail" content="{{ url('thumb-img/'. $voucher->img_id.'.png') }}">
	    <meta property="og:title" content="{{ $voucher->brand_name }} - {{ Translate::transObj($voucher, 'name') }}">
	    <meta property="og:description" content="{{ $voucher->sender_name.' '.trans('content.you_given') }}">
	    <meta property="og:url" content="{{ Request::url() }}">
	    <meta property="og:image" content="{{ url('thumb-img/'. $voucher->img_id.'.png') }}">
	    <meta property="og:image:width" content="200"/>
	    <meta property="og:image:height" content="200"/>
	    @endif
	    <link rel="shortcut icon" href="/favicon.gif">
	    <link rel="stylesheet" type="text/css" href="{!!  asset('layouts/v3/css/font-awesome.min.css')  !!}" />
		<!-- <link rel="stylesheet" type="text/css" href="{{asset('/layouts/v2/css/new_voucher_style.css')}}"> -->

		<style type="text/css">
		.clearfix{
			clear: both;
			display: block;
		}
		.menu_icon{
			left: -42px;
		    position: absolute;
		    top: 0;
		    height: 45px;
		    width: 30px;
		    width: 42px;
		    background: #fff;
		    border: 0;
		    padding: 9px 10px;
		}
		.menu_icon .icon-bar {
		    background-color: #888;
		    transition: all 0.3s ease-in-out;
		    height: 3px;
	    	display: block;
		    width: 22px;
		    border-radius: 1px;
		    margin-top: 4px;
		}
		.menu_icon .icon-bar:nth-of-type(2){
			margin-top: 0;
		}
		.menu_icon:active,
		.menu_icon:focus{
			border: 0;
			outline: none;
			box-shadow: none;
		}

		.menu_icon.showMenu .icon-bar:nth-of-type(2) {
		    -webkit-transform: translate3d(0, 7px, 0) rotate(45deg);
		    transform: translate3d(0, 7px, 0) rotate(45deg);
		    background: #ff5f5f;
		}
		.menu_icon.showMenu .icon-bar:nth-of-type(3) {
		    opacity: 0;
		    filter: alpha(opacity=0);
		}
		.menu_icon.showMenu .icon-bar:nth-of-type(4) {
		    -webkit-transform: translate3d(0, -7px, 0) rotate(-45deg);
		    transform: translate3d(0, -7px, 0) rotate(-45deg);
		    background: #ff5f5f;
		}







		.menu_icon img{
			height: 24px;
			margin-top: 8px;
			margin-left: 10px;
		}
		.menu_icon img.close_ic{
			display: none;
		}
		.menu_icon.showMenu img.close_ic{
			display: inline-block !important;
			margin-left: 13px;
		}
		.menu_icon.showMenu img.open_ic{
			display: none;
		}
		.sidenav p.hotline{
			position: absolute;
			left: 30px;
			right: 20px;
			bottom: 15px;
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: 500;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.33;
			letter-spacing: normal;
			color: #ffffff;
		}
		.sidenav p.hotline a{
			float: right;
			font-family: Gotham Rounded;
			font-size: 14px;
			font-weight: 500;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			color: #fc6063;
			text-decoration: none;
		}

		@font-face { 
			font-family: 'Gotham Rounded'; 
			src: url('../layouts/v2/layouts/v2/fonts/SVN-GothamRounded.eot') format('embedded-opentype'); 
			src: url('../layouts/v2/fonts/SVN-GothamRounded.woff') format('woff'), url('../layouts/v2/fonts/SVN-GothamRounded.ttf') format('truetype'), url('../layouts/v2/fonts/SVN-GothamRounded.svg#SVN-GothamRounded') format('svg'); 
			font-weight: normal; 
			font-style: normal; 
		}

		@font-face { 
			font-family: 'Gotham Rounded'; 
			src: url('../layouts/v2/fonts/SVN-GothamRoundedMedium.eot') format('embedded-opentype'); 
			src: url('../layouts/v2/fonts/SVN-GothamRoundedMedium.woff') format('woff'), url('../layouts/v2/fonts/SVN-GothamRoundedMedium.ttf') format('truetype'), url('../layouts/v2/fonts/SVN-GothamRoundedMedium.svg#SVN-GothamRoundedMedium') format('svg'); 
			font-weight: 500; 
			font-style: normal; 
		}
		@font-face { 
			font-family: 'fontawesome-webfont'; 
			src: url('../layouts/v2/fonts/fontawesome-webfont.eot') format('embedded-opentype'); 
			src: url('../layouts/v2/fonts/fontawesome-webfont.woff') format('woff'), url('../layouts/v2/fonts/fontawesome-webfont.ttf') format('truetype'), url('../layouts/v2/fonts/fontawesome-webfont.svg#fontawesome-webfont') format('svg'); 
			font-weight: 500; 
			font-style: normal; 
		}

		*{
			margin: 0;
			padding: 0;
			font-family: "Gotham Rounded";
		}
		html, body {
		    height: 100%;
		}
		.sidenav {
		    height: 100%;
		    width: 270px; 
		    position: fixed; 
		    z-index: 1;
		    top: 0;
		    right: -270px;
		    background-color: #494953; 
		    transition: 0.3s; 
		    color: #fff;
		    z-index: 15;
		}
		.sidenav .language{
			display: block;
			overflow: hidden;
			background: rgba(255, 255, 255, 0.1);
			height: 46px;
			line-height: 46px;
			padding-left: 30px;
		}
		.sidenav .language p{
			display: inline-block;
			float: left;
		}
		.sidenav ul{
			display: inline-block;
			float: right;
			list-style-type: none;
		}
		.sidenav .language ul li{
			float: left;
			padding: 8px 10px 8px 0;
			text-align: center;
		}
		.sidenav .language ul li a{
			height: 30px;
			width: 30px;
			line-height: 30px;
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			letter-spacing: normal;
			color: #ffffff;
			padding:0;

		}
		.sidenav .language ul li.active a{
			background: #fc6063;
			border-radius: 50%;

		}
		.sidenav .list_menu{
			overflow: hidden;
			padding-top: 15px;
		}
		.sidenav .list_menu ul{
			float: left;
			width: 100%;
		}
		.sidenav .list_menu ul li{
			padding-top: 15px;
			padding-bottom: 15px;
		}
		.sidenav .list_menu ul li a{
			position: relative;
		}
		.sidenav .list_menu ul li a::after{
			position: absolute;
		    right: 20px;
		    top: 50%;
		    transform:translateY(-50%);
		    -ms-transform: translateY(-50%); /* IE 9 */
		    -webkit-transform: translateY(-50%); /* Chrome, Safari, Opera */
		    content: '';
		    display: inline-block;
		    width: 0;
		    height: 0;
		    margin-left: 2px;
		    vertical-align: middle;
		    border-left: 5px dashed !important;
		    border-top: 5px solid transparent !important;
		    border-bottom: 5px solid transparent !important;
		}
		.sidenav .list_menu ul li a small{
			opacity: 0.3;
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			text-align: right;
			color: #ffffff;
		}
		.sidenav .list_menu ul li a small::before{
			content:"\a";
			white-space: pre;
		}
		.sidenav a {
		    padding: 0;
		    text-decoration: none;
		    font-size: 25px;
		    color: #fff;
		    display: block;
		    transition: 0.3s;
		    font-family: Gotham Rounded;
			font-size: 16px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			color: #ffffff;
			padding-left: 30px;

		}
		.sidenav a:hover, .offcanvas a:focus{
		    /*color: #f1f1f1;*/
		}
		.sidenav .closebtn {
		    position: absolute;
		    top: 0;
		    left: -55px;
		    font-size: 36px;
		}

		.fixed_btn{
			position: fixed !important;
		}
		#mySidenav.showMenu{
			/*width: auto;*/
			right: 0;
			/*left: 50px;*/
		}

		.menu{
			top: 10px;
			right: 10px;
			/*width: 20px;*/
			position: absolute;
			z-index: 13;
			transition: 1s; 
		}
		.menu.showMenu{
			width: 290px;
		}
		#voucherWrapper{
			max-width: 375px;
		    width: 100%;
		    margin: 0 auto;
		    background: #fff;
		    overflow: hidden;
		    min-height: 100%;
		    position: fixed;
		    left: 0;
		    right: 0;
		    overflow-y:auto;
		    top: 0;
		    bottom: 0;
		}
		.voucher_header{
			background: #fff;
		    padding: 0px;
		    height: 46px;
		    position: fixed;
		    top: 0;
		    left: 0;
		    right: 0;
		    max-width: 375px;
		    width: 100%;
		    z-index: 9999;
		    margin: 0 auto;

		}
		#voucherWrapper.active .voucher_header{
			box-shadow: 0 4px 4px -1px rgba(0, 0, 0, 0.18);
		}
		.voucher_header a.logo {
		    display: inline-block;
		    padding: 0;
		    position: absolute;
		    left: 50%;
		    transform:translateX(-50%);
		    -ms-transform: translateX(-50%); /* IE 9 */
		    -webkit-transform: translateX(-50%); /* Chrome, Safari, Opera */
		}
		.voucher_header a.logo img {
		    height: 45px;
		    /*vertical-align: middle;*/
		}
		.voucher_header a.back_page{
			padding: 6px 10px 7px;
			display: inline-block;
			/*display: none;*/
		}
		.voucher_header a.back_page img{
			height: 30px;
		}
		.voucher_header .pull-right{
			display: inline-block;
			float: right;
			position: relative;
		}

		.voucher_header .pull-right a.menu{
			/*padding: 5px 0;
			height: 40px;
			line-height: 40px;
			display: table-cell;
			vertical-align: middle;*/
		}
		.voucher_header .pull-right a.menu img{
			height: 24px;
		}
		.voucher_body{
			padding-bottom: 50px;
			padding-top: 46px;
		}
		.voucher_body .top_body{
			
		}
		#voucherWrapper::-webkit-scrollbar,
		.voucher_body::-webkit-scrollbar {
		    width: 0;
		}
		.welcome_page{
			/*background: #eb01a5 !important;*/
		/*	background-image: url(../layouts/v2/images/FE_img_en.png) !important;*/
			background-size: 100% auto !important;
			background-repeat: no-repeat !important; 
		}
		.welcome_page .voucher_body .top_body{
			height: 180px;
			background: url(../layouts/v2/images/new_voucher/top_bg.png) #2c6fa6;
			background-repeat: no-repeat;
			background-size: 100% 100%;

		}
		.voucher_page{
			background: #dedfe2 !important;
		}
		.voucher_page .voucher_body{
			padding-left: 10px;
			padding-right: 10px;
			margin-top: 10px;

		}

		.voucher_page .voucher_body .top_body{
			/*height: 60px;
			background: url(../layouts/v2/images/new_voucher/top_bg.png) #f2f2f3 center bottom;
			background-repeat: no-repeat;
			background-size: 100% auto;*/
		}
		.voucher_page .voucher_body .content_body{
			background: #fff;
			/*border-radius: 8px;*/
			padding:0;
		}
		.voucher_body .top_body p.message{
			text-align: center;
			padding-top: 20px;
			font-family: Gotham Rounded;
			font-size: 14px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			text-align: center;
			color: #fff;
		}
		.voucher_body .top_body p.message b{
			font-weight: 500;
		}
		.voucher_body .content_body{
			min-height: 250px;
			padding: 10px;
		}
		.product_info .img{
			height: 234px;
			width: 240px;
			margin: 0 auto;
			position: relative;
			margin-top: -110px;
			background: url('../layouts/v2/images/new_voucher/shadow_bg.png');
			background-size: 100% 100%;
		}
		.voucher_page .product_info .img{
			width: 240px;
			height: 240px;
			margin-top: 0px;
			background: none;
		}

		.product_info .img img{
			width: 100%;
			position: absolute;
			top: 50%;
			left: 50%;
			transform:translate(-50%,-50%);
			-ms-transform: translate(-50%,-50%); /* IE 9 */
		    -webkit-transform: translate(-50%,-50%); /* Chrome, Safari, Opera */
			max-width: 200px;
			margin-top: -10px;
		}
		.step_one .product_info .img img{
			max-width: 180px;
			margin-top: -17px;
		}
		.voucher_page .product_info .img img{
			/*max-width: 85px;
			margin-top: -10px;*/
			width: 100%;
		}
		.product_info  .detail{
			padding: 35px 0;
			background: url('../layouts/v2/images/new_voucher/quote.png') no-repeat center;
			background-size: 125px 110px;
			text-align: center;
			min-height: 45px;
		}
		.product_info  .detail p.message{
			max-width: 240px;
			text-align: center;
			margin: 0 auto;
			font-family: "Gotham Rounded";
			font-size: 16px;
			font-weight: normal;
			font-style: italic;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			text-align: center;
			color: #000000;

		}
		.voucher_page .product_info  .detail{
			padding: 0;
			background:none;
			margin-top: -25px;
		}
		.voucher_page .product_info  .detail p.product_name{
			font-family: Gotham Rounded;
			font-size: 16px;
			font-weight: 500;
			font-style: normal;
			font-stretch: normal;
			line-height: 24px;
			letter-spacing: normal;
			text-align: center;
			color: #000000;
		}
		.voucher_page .product_info  .detail p.product_price_size{
			font-family: Gotham Rounded;
			font-size: 14px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			line-height: 21px;
			letter-spacing: normal;
			text-align: center;
			color: #666666;
		}
		.voucher_page .product_info  .detail p.product_brand{
			font-size: 0;
			margin-top: 15px;

		}
		.voucher_page .product_info  .detail p.product_brand img{
			width: 70px;
			border-radius: 8px;
			background-color: #ffffff;
			border: solid 2px #ebebeb;
			position: relative;
			z-index: 999;
			margin-bottom: -35px;
		}
		.voucher_page .content_body .voucher_info{
			text-align: center;

		}
		.voucher_page .content_body .voucher_info .voucher_detail{
			/*background: #f6f6f6;*/
			/*padding: 5px 0px;*/
			margin: 0 auto;
			/*max-width: 300px;*/
			position: relative;
			overflow: hidden;
		}
		.voucher_page .content_body .voucher_info .voucher_detail p.notice{
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			text-align: center;
			color: #333333;
			background:rgba(0, 0, 0, 0.03);
			padding-top: 40px;
			padding-bottom: 10px;
			margin-bottom: 10px;
		}
		.voucher_page .content_body .voucher_info .voucher_detail p.code{
			font-family: Gotham Rounded;
			font-size: 24px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			line-height: normal;
			letter-spacing: 4px;
			text-align: center;
			color: #000000;
			margin: 5px auto
		}
		.voucher_page .content_body .voucher_info .voucher_detail p.qr_code img{
			width: 80px;
			height: 80px;
		}
		.wrap_code{
			padding: 5px 15px;
			overflow: hidden;
			position: relative;
			margin-bottom: 10px;
		}
		.qrCode{
			display: inline-block;
		    float: left;
		    font-size: 0;
		}
		.qrCode img{
			margin-top: 10px;
			width: 90px;
			height: 90px;
		}
		.textCode {
			display: inline-block;
		    position: absolute;
		    left: 120px;
		    /* right: 0; */
		    text-align: left;
		    /* padding-left: 130px; */
		    /* bottom: 0; */
		    /* margin-bottom: 10px; */
		    top: 50%;
		    transform: translateY(-50%);
		    -ms-transform: translateY(-50%); /* IE 9 */
		    -webkit-transform: translateY(-50%); /* Chrome, Safari, Opera */
		}
		.textCode p{
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: 500;
			line-height: 2;
			text-align: left;
			color: #000000;
			text-transform: uppercase;
		}
		
		.voucher_page .content_body .voucher_info .voucher_detail .textCode .code{
			text-align: left;
			letter-spacing: 3px;
			margin:0 0 5px 0;
		}
		.voucher_page .content_body .voucher_info .voucher_detail p.expired_date{
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: 500;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.33;
			letter-spacing: normal;
			text-align: center;
			color: #000000;
			margin-top: 10px;
		}
		.voucher_page .content_body .voucher_info .voucher_save{
			margin: 0 auto;
			border-top: 2px dashed #ebebeb;
			padding: 5px 0 10px;
			position: relative;
			margin-top: 15px;
			/*margin-top: 30px;*/
			/*max-width: 300px;*/
		}
		.voucher_page .content_body .voucher_info .voucher_save:before{
			position: absolute;
			width: 20px;
			height: 20px;
			background:#dedfe2;
			content:'';
			display: inline-block;
			border-radius: 50%;
		    left: -10px;
		    top: -10px;
		}
		.voucher_page .content_body .voucher_info .voucher_save:after{
			position: absolute;
			width: 20px;
			height: 20px;
			background:#dedfe2;
			content: '';
			display: inline-block;
			top: -10px;
		    right: -10px;
		    border-radius: 50%;
		}
		.voucher_page .content_body .voucher_info .voucher_save .save_btn{
			font-family: Gotham Rounded;
			font-size: 14px;
			font-weight: 500;
			font-style: normal;
			font-stretch: normal;
			line-height: 21px;
			letter-spacing: 1px;
			text-align: center;
			color: #333333;
			text-transform: uppercase;
			text-decoration: none;
			display: block;
			margin: 0 auto;
			/*border-radius: 6px;*/
		  	/*border: solid 2px #333333;*/
		  	height: 34px;
		  	line-height: 34px;
		  	cursor: pointer;
		}
		.copy_right{
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.75;
			letter-spacing: normal;
			text-align: center;
			color: #999999;
			margin-top: 5px;
			margin-bottom: 10px;
		}

		#voucherWrapper .voucher_footer {
		    background: #eee;
		    width: 100%;
		    color: #e0e0e0;
		    font-size: 14px;
		    position: absolute;
		    bottom: 0;
		    left: 0;
		    right: 0;
		    max-width: 375px;
		    margin: 0 auto;
		    z-index: 1000;
		}
		.voucher_footer div.footer_info {
		    margin: 0 auto;
		    display: block;
		    text-align: center;
		    height: 50px;
		    line-height: 50px;
		    font-family: Gotham Rounded;
		    font-weight: normal;
		    background: #1F1E75;
		    /*box -shadow: 0 -3px 6px 0 rgba(0, 0, 0, 0.05);*/
		}

		.voucher_footer div.footer_info a{
			text-transform: uppercase;
			font-family: Gotham Rounded;
			font-size: 14px;
			font-weight: 500;
			font-style: normal;
			font-stretch: normal;
			line-height: 50px;
			letter-spacing: 1px;
			text-align: center;
			color: #ffffff;
			text-decoration: none;
			display: block;
		}
		/*save page*/
		.save_voucher_page .voucher_body{
			background: #f6f6f6;
		}
		.save_voucher_page .voucher_body .content_body {
			padding-bottom: 25px;
		}
		.save_voucher_page .voucher_body .content_body p{
			font-family: Gotham Rounded;
			font-size: 14px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;      
			line-height: 1.5;
			letter-spacing: normal;
			text-align: center;
			color: #333333;
			max-width: 270px;
			margin: 0 auto;
		}
		.save_voucher_page .voucher_body .content_body img{
			width: 100%;
			margin-top: 10px;
		}
		/*location page*/
		.voucher_location_page{

		}
		.top_body.filter{
			padding: 0 10px;
			height: 40px;
			background: #f2f2f3;
			position:relative;
			z-index: 1000
		}
		.top_body.filter .left{
			display: inline-block;
			float: left;
		}
		.top_body.filter .left p.filter_text{
			padding-left:25px;
			background: url('../layouts/v2/images/new_voucher/filter_icon.png') no-repeat left center;
			background-size: 18px auto;
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			/*line-height: 1.33;*/
			letter-spacing: normal;
			color: #000000;
			line-height: 40px;
		}
		.top_body.filter .right{
			display: inline-block;
			float: right;
			/*position: relative;*/
		}
		.top_body.filter .all_location_btn{
			text-decoration: none;
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: 500;;
			font-style: normal;
			font-stretch: normal;
			line-height: 40px;
			letter-spacing: normal;
			color: #666666;
			padding-right: 15px;
			position: relative;
			display: inline-block;
		}
		.top_body.filter .all_location_btn::after{
			display: inline-block;
		    width: 0;
		    height: 0;
		    content:'';
		    vertical-align: middle;
		    border-top: 5px dashed;
		    border-top: 5px solid\9;
		    border-right: 5px solid transparent;
		    border-left: 5px solid transparent;
		    position: absolute;
		    top: 50%;
		    right: 0;
		    transform:translateY(-50%);
		    -ms-transform: translateY(-50%); /* IE 9 */
		    -webkit-transform: translateY(-50%); /* Chrome, Safari, Opera */
		}
		.top_body.filter .all_location_btn.open::after{
			display: inline-block;
		    width: 0;
		    height: 0;
		    content:'';
		    vertical-align: middle;
		    border-bottom: 5px dashed;
		    border-bottom: 5px solid\9;
		    border-top: none;
		    border-right: 5px solid transparent;
		    border-left: 5px solid transparent;
		    position: absolute;
		    top: 50%;
		    right: 0;
		    transform:translateY(-50%);
		    -ms-transform: translateY(-50%); /* IE 9 */
		    -webkit-transform: translateY(-50%); /* Chrome, Safari, Opera */
		}
		.top_body.filter .filter_option{
			position: absolute;
			left: 0;
			right: 0;
			display: none;
			padding: 0 10px;
			z-index: 11;
			background: #fff;
		}
		.top_body.filter .filter_option .accordion-section{
			margin-top: 30px;
		}
		.top_body.filter .filter_option .accordion-section ul li{
			padding: 3px 0;
			cursor: pointer;
		}
		.top_body.filter .filter_option .accordion-section:first-child{
			margin-top: 20px;
		}
		.top_body.filter .filter_option .accordion-section p{
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			color: #666666;
		}
		.top_body.filter .filter_option .accordion-section .accordion-section-title{
			width: 100%;
			display: block;
			border-bottom: 1px solid;
			text-decoration: none;
			position: relative;
			font-family: Gotham Rounded;
			font-size: 14px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			text-align: left;
			color: #333333;
			margin-top: 5px;
		}
		.top_body.filter .filter_option .accordion-section ul.city,
		.top_body.filter .filter_option .accordion-section ul.district{
			overflow-x:auto;
			max-height: 200px;
		}
		.top_body.filter .filter_option .accordion-section .accordion-section-title:focus,
		.top_body.filter .filter_option .accordion-section .accordion-section-title:visited{
			color: #333333;
			text-decoration: none;
		}
		.top_body.filter .filter_option .accordion-section .accordion-section-title::after{
			display: inline-block;
		    width: 0;
		    height: 0;
		    content:'';
		    vertical-align: middle;
		    border-top: 5px dashed;
		    border-top: 5px solid\9;
		    border-right: 5px solid transparent;
		    border-left: 5px solid transparent;
		    position: absolute;
		    top: 50%;
		    right: 0;
		    transform:translateY(-50%);
		    -ms-transform: translateY(-50%); /* IE 9 */
		    -webkit-transform: translateY(-50%); /* Chrome, Safari, Opera */

		}
		.top_body.filter .filter_option .accordion-section .accordion-section-title.active::after{
			display: inline-block;
		    width: 0;
		    height: 0;
		    content:'';
		    vertical-align: middle;
		    border-bottom: 5px dashed;
		    border-top: none;
		    border-right: 5px solid transparent;
		    border-left: 5px solid transparent;
		    position: absolute;
		    top: 50%;
		    right: 0;
		    transform:translateY(-50%);
		    -ms-transform: translateY(-50%); /* IE 9 */
		    -webkit-transform: translateY(-50%); /* Chrome, Safari, Opera */

		}
		.top_body.filter .all_location_btn.open + .filter_option{
			display: block;
		}
		.top_body.filter  .filter_option .apply_filter_btn{
			font-family: Gotham Rounded;
		    font-size: 14px;
		    font-weight: 500;
		    font-style: normal;
		    font-stretch: normal;
		    line-height: 21px;
		    letter-spacing: 1px;
		    text-align: center;
		    color: #333333;
		    text-transform: uppercase;
		    text-decoration: none;
		    display: block;
		    margin: 0 auto;
		    border-radius: 6px;
		    border: solid 2px #333333;
		    height: 34px;
		    line-height: 34px;
		    cursor: pointer;
		    margin: 30px auto 10px;
		}
		.wrap_filter{
			position: absolute;
			background: rgba(0, 0, 0, 0.7);
			top: 80px;
			left: 0;
			right: 0;
			bottom: 0;
			z-index: 10;
			display: none;
		}
		.wrap_filter.active{
			display: block;
		}

		/*where to use*/
		.category_page .content_body .category_list{
			text-align: center;
			margin-right: -10px;
			margin-left: -10px;
		}
		.category_page .content_body .category_list p.notice,.brand_page .content_body .brand_list p.notice{
			font-family: Gotham Rounded;
			font-size: 14px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			text-align: center;
			color: #333333;
			margin-bottom: 0px;
			padding: 0 10px;
		}
		.category_page .content_body .category_list .category_item{
			width: 33.3%;
			display: inline-block;
			float: left;
			margin-top: 15px;
		}
		.category_page .content_body .category_list .category_item a{
			text-decoration: none;
		}
		.category_page .content_body .category_list .category_item p{
			max-width: 85px;
			margin: 0 auto;
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: 500;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			text-align: center;
			color: #666666;
			height: 36px;
			overflow: hidden;
		}
		.category_page .content_body .category_list .category_item .has_new_br{
			position: relative;
			width: 82px; 
			margin: 0 auto;
			z-index: 1;

		}
		.category_page .content_body .category_list .category_item .has_new_br .new_br_ic{
			display: block;
			margin: 0 auto;
			width: 10px;
			height: 10px;
			text-indent: -1000px;
			top: 10px;
			right: 7px;
			background: #fc6063;
			position: absolute;
			z-index: 999;
			border-radius: 50%;
		}
		.category_page .content_body .category_list .note_new_br{
			margin: 0 auto;
			text-align: center;
			padding-left: 20px;
			display: inline-block;
			position: relative;
			margin-top: 30px;
			font-family: Gotham Rounded;
			font-size: 10px;
			font-weight: 500;
			font-style: normal;
			font-stretch: normal;
			line-height: normal;
			letter-spacing: normal;
			color: #333333;
			text-transform: uppercase;
		}
		.category_page .content_body .category_list .note_new_br span{
			width: 10px;
			height: 10px;
			top: 50%;
			transform:translateY(-50%);
			left: 0px;
			display: inline-block;
			margin: 0 auto;
			background: #fc6063;
			border-radius: 50%;
			position: absolute;
		}
		.category_page .content_body .category_list .category_item .wrap_icon{
			width: 78px;
			height: 78px;
			border: solid 2px #f2f2f3;
			border-radius: 50%;
			margin: 0 auto;
			position: relative;
			overflow: hidden;
			margin-bottom: 5px;
		}
		.category_page .content_body .category_list .category_item .wrap_icon span{
			position: absolute;
			bottom: 0;
			left: 0;
			right: 0;
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			line-height: normal;
			letter-spacing: normal;
			text-align: center;
			color: #666666;
			height: 18px;
			line-height: 18px;
			background: #f2f2f3;
			z-index: 1;
		}

		.category_page .content_body .category_list .category_item .wrap_icon.flower{
		    background: url(../layouts/v2/images/new_voucher/ic-flower.png);
		    background-size: 28px auto;
		    background-repeat: no-repeat;
		    background-position: center top 15px;
		}

		.category_page .content_body .category_list .category_item .wrap_icon.fastfood-cafe{
		    background: url(../layouts/v2/images/new_voucher/ic-fast-food-coffee.png);
		    background-size: 28px auto;
		    background-repeat: no-repeat;
		    background-position: center top 15px;
		}

		.category_page .content_body .category_list .category_item .wrap_icon.restaurants{
		    background: url(../layouts/v2/images/new_voucher/ic-restaurants.png);
		    background-size: 28px auto;
		    background-repeat: no-repeat;
		    background-position: center top 15px;
		}

		.category_page .content_body .category_list .category_item .wrap_icon.fragrance-cosmetic{
		    background: url(../layouts/v2/images/new_voucher/ic-comestics.png);
		    background-size: 28px auto;
		    background-repeat: no-repeat;
		    background-position: center top 15px;
		}

		.category_page .content_body .category_list .category_item .wrap_icon.fashion-cosmetic{
		    background: url(../layouts/v2/images/new_voucher/ic-fashions.png);
		    background-size: 28px auto;
		    background-repeat: no-repeat;
		    background-position: center top 15px;
		}

		.category_page .content_body .category_list .category_item .wrap_icon.entertaiment{
		    background: url(../layouts/v2/images/new_voucher/ic-entertainment.png);
		    background-size: 28px auto;
		    background-repeat: no-repeat;
		    background-position: center top 15px;
		}

		.category_page .content_body .category_list .category_item .wrap_icon.mall-mart{
		    background: url(../layouts/v2/images/new_voucher/ic-mall-mart.png);
		    background-size: 28px auto;
		    background-repeat: no-repeat;
		    background-position: center top 15px;
		}

		.category_page .content_body .category_list .category_item .wrap_icon.health-and-sport{
		    background: url(../layouts/v2/images/new_voucher/ic-health-sports.png);
		    background-size: 28px auto;
		    background-repeat: no-repeat;
		    background-position: center top 15px;
		}

		.category_page .content_body .category_list .category_item .wrap_icon.lifestyle{
		    background: url(../layouts/v2/images/new_voucher/ic-lifestyle.png);
		    background-size: 28px auto;
		    background-repeat: no-repeat;
		    background-position: center top 15px;
		}

		.category_page .content_body .category_list .category_item .wrap_icon.kitchen-dining{
		    background: url(../layouts/v2/images/new_voucher/ic-kitchen.png);
		    background-size: 28px auto;
		    background-repeat: no-repeat;
		    background-position: center top 15px;
		}

		.category_page .content_body .category_list .category_item .wrap_icon.others{
		    background: url(../layouts/v2/images/new_voucher/ic-others.png);
		    background-size: 28px auto;
		    background-repeat: no-repeat;
		    background-position: center top 15px;
		}
		.brand_page .content_body .brand_list .wrap_list{
			border-top: 1px solid #f2f2f3;
			border-bottom: 1px solid #f2f2f3;
			overflow: hidden;
			margin-top: 20px;
			margin-bottom: 20px;
		}
		.brand_page .content_body .brand_list .wrap_list.no-data{
			border: 0;

		}
		.brand_page .content_body .brand_list .wrap_list.no-data p{
			position: absolute;
			top: 50%;
			left: 50%;
			transform:translate(-50%,-50%);
		}
		.brand_page .content_body .brand_list{
			margin-right: -10px;
			margin-left: -10px;
			text-align: center;

		}

		.brand_page .content_body .brand_list .brand_item{
			width: 33.3%;
		    display: inline-block;
		    float: left;
		    margin-right: -1px;
		    margin-bottom: -1px;
		    border-right: solid 0.5px #f2f2f3;
		    border-bottom: 1px solid #f2f2f3;
		    position: relative;
		}
		.brand_page .content_body .brand_list .brand_item .new_br_ic{
			position: absolute;
		    right: 10px;
		    top: 10px;
		    background: #fc6063;
		    width: 10px;
		    height: 10px;
		    display: inline-block;
		    border-radius: 50%;
		    text-indent: -1000px;
		}
		.brand_page .content_body .brand_list .brand_item img{
			width: 100%;
		}
		.brand_page .voucher_footer{
			position: fixed !important;
			bottom: 0;
		}
		.brand_page .voucher_header{
			/*position: relative;*/
		}
		.brand_page .voucher_body .top_body p.category,
		.location_page .voucher_body .top_body p.brand_name{
			line-height: 40px;
			height: 40px;
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: 500;
			color: #666666;
		}

		.save_voucher_page .voucher_header p.save,
		.brand_page .voucher_header p.category,.location_page .voucher_header p.brand_name{
			position: absolute;
			left: 50%;
			top: 50%;
			transform:translate(-50%,-50%);
			max-width: 175px;
		    overflow: hidden;
		    font-family: Gotham Rounded;
			font-size: 14px;
			font-weight: 500;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			text-align: center;
			color: #000000;
			white-space: nowrap;
		}
		.brand_page .voucher_header p.category span{
			font-weight: normal;
		}
		.location_page .content_body{
			padding: 0;
		}
		.location_page .voucher_footer{
			position: fixed !important;
			bottom: 0;
		}
		.location_page .content_body #embed-map{
			height: 210px;
			background:#f2f2f3;
		}
		.location_page .content_body .list_store{
			display: block;
			overflow: hidden;
			margin-bottom: 20px;
		}
		.location_page .content_body .list_store .list-add .find_near{
			font-family: Gotham Rounded;
			font-size: 14px;
			font-weight: 500;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			text-align: center;
			color: #fc6063;
			margin: 20px 0;
			display: block;
			text-decoration: none;
		}
		.location_page .content_body .list_store .list-add ul li {
		    position: relative;
		    display: block;
		    width: 100%;
		    float: left;
		    border-bottom: 1px #f4f4f4 solid;
		    padding: 10px 0;
		    background: #f6f6f6;
		    margin-top: 5px;
		}
		.location_page .content_body .list_store .list-add ul li.active{
			background: #e3e3e3;
		}
		.location_page .content_body .list_store .list-add ul li p{
			font-family: Gotham Rounded;
			font-size: 14px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			text-align: left;
			padding-right: 45px;
		    padding-left: 10px;
			color: #333333;
		}
		.location_page .content_body .list_store .list-add ul li p span{
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			color: #666666;
		}
		.location_page .content_body .list_store .list-add ul li p img {
		    position: absolute;
		    width: 24px;
		    right: 10px;
		    top: 50%;
		    transform:translateY(-50%);
		}


		.m_wrap{
			background: #fff;
			border: 0;
			border-radius: 6px;
			box-shadow: 0 5px 6px 0 rgba(0, 0, 0, 0.2);
		  	border: solid 1px #fff;
			width: 245px !important;
			margin-left: 25px;
			margin-top: -10px;
			min-height: 110px !important;
		}
		.m_wrap .gm-style-iw {
		   width: auto !important;
		   top: 0 !important;
		   left: 0 !important;
		   right: 0 !important;

		}
		.m_wrap .gm-style-iw div{
			width: 100%;
			max-width: initial !important;
			max-height: none !important;
			/*overflow: initial !important;*/
		}

		.m_wrap .wrap_marker h3{
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: 500;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.33;
			letter-spacing: normal;
			color: #000000;
			padding: 10px 20px 0px;
			white-space: nowrap;
			overflow: hidden;
		}
		.m_wrap .wrap_marker p{
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			color: #666666;
			padding: 5px 20px;

		}
		.m_wrap .wrap_marker p.address{
			height: 36px;
			overflow: hidden;
		}
		.m_wrap .wrap_marker a{
			text-decoration: none;
		}
		.m_wrap .wrap_marker p.get_direction{
			padding-left: 45px;
			background: url(../layouts/v2/images/new_voucher/direct.png) no-repeat center left 20px;
			background-size: 18px 18px;
			font-family: Gotham Rounded;
			font-size: 14px;
			font-weight: 500;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			color: #4f67fe;
		}
		.m_wrap .close_btn{
			right: 7px !important;
			top: 10px !important;
			left: initial !important;
		}
		.m_wrap::before {
		    content: '';
		    display: block;
		    top: 100%;
		    width: 0;
		    height: 0;
		    position: absolute;
		    left: 50%;
		    margin-left: -8px;
		    border-left: 8px solid transparent;
		    border-right: 8px solid transparent;
		    border-top: 8px solid #fff;
		}
		.m_wrap::after {
		    content: '';
		    display: block;
		    top: 100%;
		    width: 0;
		    height: 0;
		    position: absolute;
		    left: 50%;
		    margin-left: -8px;
		    margin-top: -2px;
		    border-left: 8px solid transparent;
		    border-right: 8px solid transparent;
		    border-top: 8px solid #fff;
		    
		}

		.loading{
			position: absolute;
			background: rgba(0, 0, 0, 0.2);
			top: 0px;
			left: 0;
			right: 0;
			bottom: 0;
			z-index: 100;
			display: none;
		}
		.loading img{
			position: absolute;
			left: 50%;
			top: 50%;
			transform:translate(-50%,-50%);
			width: 50px;
			height: 50px;
		}

		.code_section{
			padding-top: 8px !important;
			padding-bottom: 8px !important;
		}
		.code_section .logoGotitXc img{
			height: 25px;
		}

		@media (max-width: 479px){
			#voucherWrapper {
			    max-width: none;
			    width: 100%;
			}
			#voucherWrapper .voucher_footer{
				max-width: none;
			}
			.voucher_header{
				max-width: inherit;
			}
			.welcome_page .voucher_body .top_body{
				height: 200px;
			}
			.product_info .img{
				width: 305px;
				height: 297px;
				margin-top: -135px;
			}
			
		}
		@media (max-width: 375px){
			
			.voucher_page .content_body .voucher_info .voucher_detail p.qr_code img{
				width: 100px;
				height: 100px;
			}
			
			
		}
		@media (max-width: 320px){
			.product_info .img{
				width: 240px;
				height: 234px;
				margin-top: -115px;
			}
			.voucher_page .product_info  .detail p.product_name{
				font-size: 16px;
				line-height: 24px;
			}
			.textCode {
				left: 110px;
			}
			.qrCode img {
			    margin-top: 10px;
			    width: 85px;
			    height: 85px;
			}
			.voucher_page .content_body .voucher_info .voucher_detail .textCode .code{
				font-size: 22px;
				letter-spacing: 2px;
			}
			.voucher_page .product_info  .detail p.product_price_size{
				font-size: 14px;
				line-height: 21px;
			}
			.sidenav .list_menu ul li{
				padding-top: 10px;
				padding-bottom: 10px;
			}
			.voucher_page .product_info .img{
				width: 190px;
				height: 190px;
			}
			.welcome_page .voucher_body .top_body{
				height: 180px;
			}
			.voucher_page .product_info .img img{
				max-width: 150px;
			}
		}

		.link_change{
			font-size: 12px;
			font-weight: 500;
			text-transform: uppercase;
			margin-bottom: 10px;
			font-family: Gotham Rounded;
			line-height: 1.5;
			letter-spacing: normal;
			text-align: center;
			color: #333333;
			background: rgba(0, 0, 0, 0.03);
			padding-bottom: 10px;
			padding-top: 40px;
			margin-bottom: 15px;
		}
		.link_change small{
			font-family: Gotham Rounded;
			font-size: 12px;
			font-weight: normal;
			font-style: normal;
			font-stretch: normal;
			line-height: 1.5;
			letter-spacing: normal;
			text-align: center;
			color: #333333;
			/*background: rgba(0, 0, 0, 0.03);*/
			/*padding-top: 40px;*/
			padding-bottom: 2px;
			display: block;
			clear: both;
			text-transform: initial;
		}
		.link_change a{
			color: #ff5f5f;
			text-decoration: none;
			border-bottom: 1px solid #ff5f5f;
		}
		</style>
		<?php
	        $browserLang = Request::server('HTTP_ACCEPT_LANGUAGE');
	        $langPos = strpos($browserLang, 'vi');

	        if($langPos === false){
	            $browserLang = 'en';
	        }else{
	            $browserLang = 'vi';
	        }

	        $lang = Cookie::get('laravel_language', $browserLang);
	    ?>
	</head>
	<body>
		<?php //dd($voucher)?>
		
		 {{--@foreach($product->districts()->get() as $district)
	            <li id="{{ $district->dist_id }}">{{ Translate::transObj($district, 'name') }}</li>
	        @endforeach--}}
		<div id="mySidenav" class="sidenav">
			<button type="button" class="navbar-toggle collapsed menu_icon"  data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="language">
				<p>{{trans('content.language')}}</p>
				<ul>
					<li class=""><a data-lang="en" href="javascript:void(0)">EN</a></li>
					<li ><a data-lang="vi" href="javascript:void(0)">VI</a></li>
				</ul>
			</div>
			<div class="list_menu">
				<ul>
					<li><a href="https://www.gotit.vn/egift#faqs">{{trans('content.how_use_vc')}}</a></li>
					<li><a href="https://www.gotit.vn/faq.html">{{trans('content.faq_v')}}</a></li>
					<li><a href="https://www.gotit.vn/product.html">{{trans('content.popular_gift_v')}}</a></li>
					<li><a href="https://biz.gotit.vn/">{{trans('content.gotit_for_bussiness')}}<small>www.biz.gotit.vn</small></a></li>
					<li><a href="https://www.gotit.vn/">{{trans('content.what_is_gotit')}}<small>www.gotit.vn</small></a></li>
					<li><a href="https://www.facebook.com/quatangGotIt">{{trans('content.follow_us')}}<small><img src="../layouts/v2/images/new_voucher/ic-facebook.png" style="width:14px;height:14px;vertical-align: middle;"> /gotit.vn</small></a></li>
				</ul>
			</div>
			<p class="hotline">Hotline <a href="tel:1900558820">1900 5588 20</a></p>
		</div>
		<style type="text/css">
		/*.wrap{
			position: relative;
			overflow: auto;
		}*/
		</style>
		<section id="voucherWrapper" class="step_one welcome_page active" 
			style="transition: 0.3s;left:0;display: none;">
			<div class="fixed_btn voucher_footer" style="z-index:10">
				<div class="footer_info ">
					<a href="javascript:void(0)" class="open_gift_btn">{{trans('content.open_gift')}}</a>
				</div>
			</div>
		</div>
	</section>
	<section id="voucherWrapper" class="step_two voucher_page" style="transition: 0.3s;left:0;">
		<div class="voucher_header" style="transition: 0.3s;left:0;">
			<!-- <a href="javascript:void(0)" class="return_welcome_page back_page"><img src="../layouts/v2/images/new_voucher/arrow_back.png"></a> -->
			<a class="logo" href="#"><img src="https://img.gotit.vn/philip_morris_logo.png"></a>
		</div>
		<div class="voucher_body">
			<div class="top_body">
			</div>
			<div class="content_body">
				<div class="product_info">
					<div class="img">
						@if($voucher->state == 4)
								<p style="position:absolute;
											font-family:Gotham Rounded;
											top:50%;
											left:50%;
											transform:translate(-50%,-50%);
											padding:3px 10px;
											background:#fc6063;
											font-size: 14px;
											z-index:9;
											border-radius:14px;
											font-size: 10px;
											font-weight: 500;
											font-style: normal;
											font-stretch: normal;
											line-height: normal;
											letter-spacing: normal;
											color: #ffffff;
											white-space: nowrap;
											text-transform:uppercase;
											margin-top: -15px;
											">
									{{trans('content.used')}}</p>
							@endif
							@if($voucher->state == 8)
								<p style="position:absolute;
									font-family:Gotham Rounded;
									top:50%;
									left:50%;
									transform:translate(-50%,-50%);
									padding:3px 10px;
									background:#e4d20f;
									color:#000;
									z-index:9;
									text-align: right;
									border-radius:14px;
									font-size: 10px;
									font-weight: normal;
									font-style: normal;
									font-stretch: normal;
									line-height: normal;
									letter-spacing: normal;
									white-space: nowrap;
									text-transform:uppercase;
									margin-top: -15px;
									">
									{{trans('content.v_expired')}}</p>
							@endif
						<img src="{{env('IMG_SERVER').$voucher->img_path}}" style="{{($voucher->state == 4 || $voucher->state == 8) ? 'opacity:0.3;':''}}" >
					</div>
					<div class="detail">
						<p class="product_name">{{ Translate::transObj($product, 'name')   }}</p>
						@if($psize != null)
						<p class="product_price_size">{{$psize}}</p>
						@endif
						<p class="product_vallid">{{ trans('content.v_validity').": ".date('d/m/Y', strtotime($voucher->expired_date))}}</p>
						<p class="product_brand">
							<img src="https://img.gotit.vn/philip_morris_logo.png">
						</p>
					</div>
				</div>
				<div class="voucher_info">
                <div class="voucher_detail">
                    <div class="link_change" style="{{$lang == 'vi'?'display:block':'display:none'}}">
                        <p>{{trans('content.enter_phone_number')}}</p>
                    </div>
                    <div class="link_change" style="{{$lang != 'vi'?'display:block':'display:none'}}">
                         <p>{{trans('content.enter_phone_number')}}</p>
                    </div>
                    @include('voucher.topup_section',['display'=>true,'voucherNewDesign'=>true])
                </div>
            </div>
        </div>
        <p class="copy_right">Hotline: <a href="tel:1900558820">1900 5588 20</a></p>
    </div>
		<div class="fixed_btn voucher_footer" style="right:-100%;left:initial;display: none">
			<div class="footer_info ">
				<a href="javascript:void(0)" class="where_use_btn {{($vc_gotit == true) ? 'vc_multi':'vc_normal'}}" data-brand="{{$voucher->brand_id}}" >{{trans('content.where_to_use')}}</a>
			</div>
		</div>
	</div>
	</section>
	<section id="voucherWrapper" class="save_voucher_page" style="right:-100%;left:initial">
		<div class="voucher_header" style="right:-100%;left:initial">
			<a href="javascript:void(0)" class="return_voucher_page back_page"><img src="../layouts/v2/images/new_voucher/arrow_back.png"></a>
			<a class="logo" href="#"><img src="https://img.gotit.vn/philip_morris_logo.png"></a>
		</div>
		<div class="voucher_body">
			<div class="top_body">
			</div>
			<div class="content_body">
				<p>{{trans('content.save_img_note')}}</p>
				<img class="img_vc" src="">
			</div>
		</div>
		<div class="fixed_btn voucher_footer" style="right:-100%;left:initial">
			<div class="footer_info ">
				<a href="javascript:void(0)" class="return_voucher_btn" data-brand="{{$voucher->brand_id}}" >{{trans('content.return_to_voucher')}}</a>
			</div>
		</div>
		
	</div>
	</section>
<style type="text/css" media="screen">
 .detail .product_vallid{
        font-family: Gotham Rounded;
        font-size: 12px;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: center;
        color: #333333;

   }
    .form-topup:before, .form-topup:after {
            bottom: 35px
     }  
     .form-topup{
        padding-bottom: 6px;
     } 
    .phontopup .send_ant_btn{
        background: none;
        color: #000;
        border-top:2px dashed #ebebeb;
        width: 100%;
        text-align: center;
        padding-top: 3px;
        margin-top: 15px;
        font-size: 14px !important;
        font-weight: 500;

    }
    .link_change p{
        text-transform: none;
        padding: 15px;
        font-family: Gotham Rounded;
        font-size: 12px;
        text-align: center;
        font-weight: normal;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: normal;
        text-align: center;
        color: #333333;
    }
    .copy_right span{
        color:#337ab7;
    }
</style>
	<section id="voucherWrapper" class="step_four category_page" style="right:-100%;left:initial">
		<div class="voucher_header" style="right:-100%;left:initial">
			<a href="javascript:void(0)" class="return_voucher_page back_page"><img src="../layouts/v2/images/new_voucher/arrow_back.png"></a>
			<a class="logo" href="#"><img src="https://img.gotit.vn/philip_morris_logo.png"></a>
			<!-- <div class="pull-right">
				<a href="javascript:void(0)" class="menu">
					<img src="../layouts/v2/images/new_voucher/menu_icon.png">
				</a>
			</div> -->
		</div>
		<div class="voucher_body">
			<div class="top_body">
			</div>
			<div class="content_body">

				<div class="category_list">
					<p class="notice">{{trans('content.pl_choose_category')}}</p>
					<?php $check_new_br= false;?>
					@if(!empty($listCategory))
						@foreach($listCategory as $category)
						
							<div class="category_item">
								<a href="javscript:void(0)" data-id="{{$category[0]->category_id}}" data-name="{{Translate::transObj($category[0],'cate_name')}}" data-total="{{$category->count()}}">
									<div class="has_new_br" style="position:relative;">
										<?php $new_brand = false;
											$date = (\Carbon\Carbon::now()->subDays(env('SET_DATE_NEWBRAND')));
											foreach($category as $item){
												if($item->brand_created_at > $date){
													$new_brand = true;
													$check_new_br = true;
													break;
												}
											}
										?>
										
										@if($new_brand == true)
											<span class="new_br_ic"></span>
										@endif
									</div>
									<div class="wrap_icon {{$category[0]->name_slug}}">
										<span>{{$category->count()}}</span>
									</div>
									<p><?php 

										if(strlen(Translate::transObj($category[0],'cate_name')) > 20){
											echo mb_substr(Translate::transObj($category[0],'cate_name'),0,17).'...';
										}
										else{
											echo Translate::transObj($category[0],'cate_name');
										}
									?></p>
								</a>
							</div>
						@endforeach

					@endif
					<div class="clearfix"></div>
					@if($check_new_br == true)
						<p class="note_new_br"><span></span>{{trans('content.new_brands')}}</p>
					@endif
				</div>
			</div>
		</div>
		<div class="fixed_btn voucher_footer" style="right:-100%;left:initial">
			<div class="footer_info ">
				<a href="javascript:void(0)" class="return_voucher_btn">{{trans('content.return_to_voucher')}}</a>
			</div>
		</div>
		<div class="wrap_filter"></div>
	</div>
	</section>
	<section id="voucherWrapper" class="step_three brand_page" style="right:-100%;left:initial">
		<div class="voucher_header" style="right:-100%;left:initial">
			<a href="javascript:void(0)" class="return_category_page back_page"><img src="../layouts/v2/images/new_voucher/arrow_back.png"></a>
			<a class="logo" href="#"><img src="https://img.gotit.vn/philip_morris_logo.png"></a>
			<!-- <div class="pull-right">
				<a href="javascript:void(0)" class="menu">
					<img src="../layouts/v2/images/new_voucher/menu_icon.png">
				</a>
			</div> -->
		</div>
		<div class="voucher_body">
			<input type="hidden" name="cate_selected" value="">
			<div class="top_body filter">
				<div class="left">
					<p class="category">Restaurants<span>(123)</span></p>
					<input type="hidden" name="store_group_id" value="{{($product->store_group_id) ? $product->store_group_id : '0' }}">
					<!-- <p class="filter_text">{{trans('content.filter')}}</p> -->
				</div>
				<div class="right">
					<a href="javascript:void(0)" class="all_location_btn">{{trans('content.all_location')}}</a>
					<div class="filter_option">
						<div class="accordion">
							<div class="accordion-section">
								<p>{{ trans('content.city') }}</p>
								<a class="accordion-section-title" href="#accordion-1">{{trans('content.all_city')}}</a>
								<div id="accordion-1" class="accordion-1 accordion-section-content" style="display: none;">
									<ul class="city">
										
	                                </ul>
								</div><!--end .accordion-section-content-->
							</div><!--end .accordion-section-->

							<div class="accordion-section">
								<p>{{ trans('content.district') }}</p>
								<a class="accordion-section-title" href="#accordion-2">{{ trans('content.choose_district') }}</a>
								<div id="accordion-2" class="accordion-2 accordion-section-content" style="display: none;">
									<ul class="district">
	                                    
	                                </ul>
								</div><!--end .accordion-section-content-->
							</div><!--end .accordion-section-->
						</div>
						<a href="javascript:void(0)" class="apply_filter_btn">{{trans('content.apply_filter')}}</a>
					</div>	
				</div>

			</div>
			<div class="content_body">
				<div class="brand_list">
					<p class="notice">{{trans('content.pl_choose_brand')}}</p>
					<input type="hidden" name="store_group_id" value="{{($product->store_group_id) ? $product->store_group_id : '0' }}">
					<div class="wrap_list">
						<div class="brand_item">
							<a href="javascript:void(0)" data-id="10"><img class="logo-brand" src="https://img.gotit.vn/compress/brand/2015/10/1445830535_VsQfT.png"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="fixed_btn  voucher_footer" style="right:-100%;left:initial">
			<div class="footer_info ">
				<a href="javascript:void(0)" class="return_voucher_btn">{{trans('content.return_to_voucher')}}</a>
			</div>
		</div>
		<div class="wrap_filter"></div>
	</div>
	</section>
	<section id="voucherWrapper" class="step_three location_page" style="right:-100%;left:initial">
		<div class="voucher_header" style="right:-100%;left:initial">
			<a href="javascript:void(0)" class="{{($vc_gotit == true) ? 'return_brand_page':'return_voucher_page'}} back_page"><img src="../layouts/v2/images/new_voucher/arrow_back.png"></a>
			<a class="logo" href="#"><img src="https://img.gotit.vn/philip_morris_logo.png"></a>
			
			<!-- <div class="pull-right">
				<a href="javascript:void(0)" class="menu">
					<img src="../layouts/v2/images/new_voucher/menu_icon.png">
				</a>
			</div> -->
		</div>

		<div class="voucher_body">
			<input type="hidden" name="brand_selected" value="">
			<div class="top_body filter">
				<div class="left">
					<p class="brand_name">{{$voucher->brand_name}}</p>
				</div>
				<div class="right">
					<a href="javascript:void(0)" class="all_location_btn">{{trans('content.all_location')}}</a>
					<div class="filter_option">
						<div class="accordion">
							<div class="accordion-section">
								<p>{{trans('content.city')}}</p>
								<a class="accordion-section-title" href="#accordion-1">{{trans('content.all_city')}}</a>
								<div id="accordion-1" class="accordion-1 accordion-section-content" style="display: none;">
									<ul class="city">
										
									</ul>
								</div><!--end .accordion-section-content-->
							</div><!--end .accordion-section-->

							<div class="accordion-section">
								<p>{{trans('content.district')}}</p>
								<a class="accordion-section-title" href="#accordion-2">{{trans('content.choose_district')}}</a>
								<div id="accordion-2" class="accordion-2 accordion-section-content" style="display: none;">
									<ul class="district">
										
									</ul>
								</div><!--end .accordion-section-content-->
							</div><!--end .accordion-section-->
						</div>
						<a href="javascript:void(0)" class="apply_filter_btn">{{trans('content.apply_filter')}}</a>
					</div>	
				</div>

			</div>
			<div class="content_body">
				<div id="embed-map"></div>
				<div class="list_store">
					<div class="list-add">
						<input name="brand_id_choose" type="hidden" value="">
						<a href="javascript:void(0)" class="find_near" data-brandid="{{$voucher->brand_id}}" data-storegroupid ="{{ $product->store_group_id }}">{{trans('content.find_near')}}</a>
						<ul>
							<?php $number = 0;?>
	                        @foreach($stores as $store)
	                        <li>
	                            <p class="goto-location" data-number="<?php echo $number ?>" style="cursor: pointer;">
	                                {{ Translate::transObj($store, 'brand_name') . " - " . Translate::transObj($store, 'name') }}<br/>
	                                <span>{{ Translate::transObj($store, 'address') }}</span><br>
	                                <span>{{ trans('content.phone')}}: {{$store->phone}}</span>
	                                <?php $browser = strtolower($_SERVER['HTTP_USER_AGENT']);?>
									@if(stripos($browser,'iphone') !== false || stripos($browser,'ipad') !== false)
										<a href="comgooglemaps://?daddr={{Translate::transObj($store,'address')}}&directionsmode=driving">
									@else
										<a href="google.navigation:q={{Translate::transObj($store,'address')}}&{{$store->lat}},{{$store->long}}">
									@endif
									<img src="../layouts/v2/images/new_voucher/direct.png"></a>
	                            </p>
	                        </li>
	                        <?php $number++;?>
	                        @endforeach
			            </ul>
					</div>
				</div>

			</div>
		</div>
		<div class="fixed_btn voucher_footer" style="right:-100%;left:initial">
			<div class="footer_info ">
				<a href="javascript:void(0)" class="return_voucher_btn">{{trans('content.return_to_voucher')}}</a>
			</div>
		</div>
		<div class="wrap_filter"></div>
	</div>
	<input name="img_server" type="hidden" value="{{ env('IMG_SERVER')}}">
	</section>
	<div class="loading">
	  <!-- <img src="../layouts/v2/images/loading_icon.gif"> -->
	  <div id='img-loading' class='uil-spin-css' style="-webkit-transform:scale(0.4)"><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div></div>
	</div>
	<style type="text/css">
	.loading {
	    position: fixed;
	    background: rgba(0, 0, 0, 0.3);
	    top: 0px;
	    left: 0;
	    right: 0;
	    bottom: 0;
	    z-index: 100;
	    display: none;
	}
	.loading img {
	    position: absolute;
	    left: 50%;
	    top: 50%;
	    transform: translate(-50%,-50%);
	    width: 80px;
	    height: 80px;
	}
	/*Loading icon*/
	#img-loading{
	    display: block;
	    position: fixed;
	    left: 50%;
	    top: 50%;
	    margin-left: -100px;
	    margin-top: -100px;
	    z-index:9999;
	    /*background: rgba(0, 0, 0, 0.3);
	    top: 0px;
	    left: 0;
	    right: 0;
	    bottom: 0;*/
	}
	#img-loading img{
	    width: 75px;
	    height: 75px;
	}
	.uil-spin-css {
	  background: none;
	  position: relative;
	  width: 200px;
	  height: 200px;
	}
	@-webkit-keyframes uil-spin-css {
	  0% {
	    opacity: 1;
	    -webkit-transform: scale(3);
	    transform: scale(3);
	  }
	  100% {
	    opacity: 0.1;
	    -webkit-transform: scale(1);
	    transform: scale(1);
	  }
	}
	@-moz-keyframes uil-spin-css {
	  0% {
	    opacity: 1;
	    -webkit-transform: scale(3);
	    transform: scale(3);
	  }
	  100% {
	    opacity: 0.1;
	    -webkit-transform: scale(1);
	    transform: scale(1);
	  }
	}
	@-webkit-keyframes uil-spin-css {
	  0% {
	    opacity: 1;
	    -webkit-transform: scale(3);
	    transform: scale(3);
	  }
	  100% {
	    opacity: 0.1;
	    -webkit-transform: scale(1);
	    transform: scale(1);
	  }
	}
	@-o-keyframes uil-spin-css {
	  0% {
	    opacity: 1;
	    -webkit-transform: scale(3);
	    transform: scale(3);
	  }
	  100% {
	    opacity: 0.1;
	    -webkit-transform: scale(1);
	    transform: scale(1);
	  }
	}
	@keyframes uil-spin-css {
	  0% {
	    opacity: 1;
	    -webkit-transform: scale(3);
	    transform: scale(3);
	  }
	  100% {
	    opacity: 0.1;
	    -webkit-transform: scale(1);
	    transform: scale(1);
	  }
	}
	.uil-spin-css > div {
	  width: 16px;
	  height: 16px;
	  margin-left: 8px;
	  margin-top: 8px;
	  position: absolute;
	}
	.uil-spin-css > div > div {
	  width: 100%;
	  height: 100%;
	  border-radius: 100px;
	  background: #ff5f5f;
	}
	.uil-spin-css > div:nth-of-type(1) > div {
	  -webkit-animation: uil-spin-css 1s linear infinite;
	  animation: uil-spin-css 1s linear infinite;
	  -webkit-animation-delay: -0.87s;
	  animation-delay: -0.87s;
	}
	.uil-spin-css > div:nth-of-type(1) {
	  -webkit-transform: translate(84px, 84px) rotate(45deg) translate(70px, 0);
	  transform: translate(84px, 84px) rotate(45deg) translate(70px, 0);
	}
	.uil-spin-css > div:nth-of-type(2) > div {
	  -webkit-animation: uil-spin-css 1s linear infinite;
	  animation: uil-spin-css 1s linear infinite;
	  -webkit-animation-delay: -0.75s;
	  animation-delay: -0.75s;
	}
	.uil-spin-css > div:nth-of-type(2) {
	  -webkit-transform: translate(84px, 84px) rotate(90deg) translate(70px, 0);
	  transform: translate(84px, 84px) rotate(90deg) translate(70px, 0);
	}
	.uil-spin-css > div:nth-of-type(3) > div {
	  -webkit-animation: uil-spin-css 1s linear infinite;
	  animation: uil-spin-css 1s linear infinite;
	  -webkit-animation-delay: -0.62s;
	  animation-delay: -0.62s;
	}
	.uil-spin-css > div:nth-of-type(3) {
	  -webkit-transform: translate(84px, 84px) rotate(135deg) translate(70px, 0);
	  transform: translate(84px, 84px) rotate(135deg) translate(70px, 0);
	}
	.uil-spin-css > div:nth-of-type(4) > div {
	  -webkit-animation: uil-spin-css 1s linear infinite;
	  animation: uil-spin-css 1s linear infinite;
	  -webkit-animation-delay: -0.5s;
	  animation-delay: -0.5s;
	}
	.uil-spin-css > div:nth-of-type(4) {
	  -webkit-transform: translate(84px, 84px) rotate(180deg) translate(70px, 0);
	  transform: translate(84px, 84px) rotate(180deg) translate(70px, 0);
	}
	.uil-spin-css > div:nth-of-type(5) > div {
	  -webkit-animation: uil-spin-css 1s linear infinite;
	  animation: uil-spin-css 1s linear infinite;
	  -webkit-animation-delay: -0.37s;
	  animation-delay: -0.37s;
	}
	.uil-spin-css > div:nth-of-type(5) {
	  -webkit-transform: translate(84px, 84px) rotate(225deg) translate(70px, 0);
	  transform: translate(84px, 84px) rotate(225deg) translate(70px, 0);
	}
	.uil-spin-css > div:nth-of-type(6) > div {
	  -webkit-animation: uil-spin-css 1s linear infinite;
	  animation: uil-spin-css 1s linear infinite;
	  -webkit-animation-delay: -0.25s;
	  animation-delay: -0.25s;
	}
	.uil-spin-css > div:nth-of-type(6) {
	  -webkit-transform: translate(84px, 84px) rotate(270deg) translate(70px, 0);
	  transform: translate(84px, 84px) rotate(270deg) translate(70px, 0);
	}
	.uil-spin-css > div:nth-of-type(7) > div {
	  -webkit-animation: uil-spin-css 1s linear infinite;
	  animation: uil-spin-css 1s linear infinite;
	  -webkit-animation-delay: -0.12s;
	  animation-delay: -0.12s;
	}
	.uil-spin-css > div:nth-of-type(7) {
	  -webkit-transform: translate(84px, 84px) rotate(315deg) translate(70px, 0);
	  transform: translate(84px, 84px) rotate(315deg) translate(70px, 0);
	}
	.uil-spin-css > div:nth-of-type(8) > div {
	  -webkit-animation: uil-spin-css 1s linear infinite;
	  animation: uil-spin-css 1s linear infinite;
	  -webkit-animation-delay: -0s;
	  animation-delay: -0s;
	}
	.uil-spin-css > div:nth-of-type(8) {
	  -webkit-transform: translate(84px, 84px) rotate(360deg) translate(70px, 0);
	  transform: translate(84px, 84px) rotate(360deg) translate(70px, 0);
	}

	</style>
	<script
	src="https://code.jquery.com/jquery-3.1.1.min.js"
	integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
	crossorigin="anonymous"></script>
	<script type="text/javascript" src="{{asset('/layouts/v2/js/accordion.js')}}"></script>
	<script type="text/javascript" src="{!! asset('layouts/v2/js/jquery.cookie.js') !!}"></script>
	<!-- <script type="text/javascript" src="{{asset('/layouts/v2/js/new_voucher.js')}}"></script> -->
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<script type="text/javascript">
	    $.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	</script>

	<script type="text/javascript">

	$(document).ready(function(){
		var img_height = $('img.netis_img').height();
		var height_wd = $(window).height() - 60;   // returns height of browser viewport

		if(height_wd <= img_height){
			$('img.netis_img').css({'height':height_wd,'display':'block'});
		}
		else{
			$('img.netis_img').css({'display':'block'});
		}
		var lang = $('html').attr('lang');
		$('.language ul li').removeClass('active');
		$('.language ul li').find('a[data-lang="'+lang+'"]').parent().addClass('active');
		
		if($(window).width() > 414){
	        var w = ($(window).width() - $("#voucherWrapper").width())/2;
	        $('.menu_icon').css({'left':- (w + 42)+'px'});
	    }
	    else{
	        var w = 0;
	        $('.menu_icon').css({'left':'-42px'});
	    }
	    


		$('.menu_icon').click(function(e){
			e.preventDefault();
			$(this).toggleClass('showMenu');
			$('#mySidenav').toggleClass('showMenu');
			var wid = $('body').width();
			var right = (wid - $('#voucherWrapper').width())/2;
			if($('#mySidenav').hasClass('showMenu')){
				$('.menu_icon').css({'left':'-42px'});
				$('.sidenav').css({'width':wid -42 + 'px','right':'0','transition': '0.3s'});
				$('#voucherWrapper.active').css({'right':(wid-42) + 'px','left':'initial','transition': '0.3s'});
				$('#voucherWrapper.active .voucher_header').css({'right':(wid-42) + 'px','transition': '0.3s'});
				$('#voucherWrapper.active .voucher_footer').css({'right':(wid-42) + 'px','transition': '0.3s'});
				$('.menu_icon').css({'left':'-42px'});
			}
			else{
				$('.menu_icon').css({'left':-(right+42) +'px'});
				$('.sidenav').css({'right':-(wid-42) +'px'});
				$('#voucherWrapper.active').css({'right':+right+'px','transition': '0.3s'});
				$('#voucherWrapper.active .voucher_header').css({'right':+right+'px','left':'initial','transition': '0.3s'});
				$('#voucherWrapper.active .voucher_footer').css({'right':+right+'px','left':'initial','transition': '0.3s'});
			}
		});


		$('body').on('click','.all_location_btn',function(e){
			e.preventDefault();
			$(this).toggleClass('open');
			$('.accordion-section-content').removeClass('open').css({'display':'none'});
			$('.wrap_filter').toggleClass('active');
		});
		$('body').on('click','.open_gift_btn',function(e){
			e.preventDefault();
			
			if($(window).width() > 414){
	            var w = ($(window).width() - $("#voucherWrapper").width())/2;
	        }
	        else{
	            var w = 0;
	        }
	        $('.welcome_page').removeClass('active');
			$('.welcome_page').css({'right':'100%','left':'initial','transition':'0.3s'});
			$('.welcome_page .voucher_header').css({'right':'100%','left':'initial','transition':'0.3s'});
			$('.welcome_page .voucher_footer').css({'right':'100%','left':'initial','transition':'0.3s'});
			
			$('.voucher_page').addClass('active');
			$('.voucher_page').css({'right':+w+'px','left':'initial','transition':'0.3s'});
			$('.voucher_page .voucher_header').css({'right':+w+'px','left':'initial','transition':'0.3s'});
			$('.voucher_page .voucher_footer').css({'right':+w+'px','left':'initial','transition':'0.3s'});
			$('.back_page').css({'display':'inline-block'});
			$('#mySidenav').css({'display':'inline-block'});
		});

		$('body').on('click','.back_page',function(e){

			if($(window).width() > 414){
	            var w = ($(window).width() - $("#voucherWrapper").width())/2;
	        }
	        else{
	            var w = 0;
	        }
			if($(this).hasClass('return_welcome_page')){
				// $(this).css({'display':'none'});
				$(this).closest('section').removeClass('active');
				$(this).closest('section').css({'right':'-100%','left':'initial','transition': '0.3s'});
				$(this).closest('.voucher_header').css({'right':'-100%','left':'initial','transition': '0.3s'});
				$(this).closest('section').find('.voucher_footer').css({'right':'-100%','left':'initial','transition': '0.3s'});
				

				$('.welcome_page').addClass('active');
				$('.welcome_page').css({'right':+w+'px','left':'initial','transition': '0.3s'});
				$('.welcome_page .voucher_header').css({'right':+w+'px','left':'initial','transition': '0.3s'});
				$('.welcome_page .voucher_footer').css({'right':+w+'px','left':'initial','transition': '0.3s'});
				$('#mySidenav').css({'display':'none'});
			}
			if($(this).hasClass('return_voucher_page')){
				// $(this).css({'display':'none'});
				$(this).closest('section').removeClass('active');
				$(this).closest('section').css({'right':'-100%','left':'initial','transition': '0.3s'});
				$(this).closest('.voucher_header').css({'right':'-100%','left':'initial','transition': '0.3s'});
				$(this).closest('section').find('.voucher_footer').css({'right':'-100%','left':'initial','transition': '0.3s'});
				
				$('.voucher_page').addClass('active');
				$('.voucher_page').css({'right':+w+'px','left':'initial','transition': '0.3s'});
				$('.voucher_page .voucher_header').css({'right':+w+'px','left':'initial','transition': '0.3s'});
				$('.voucher_page .voucher_footer').css({'right':+w+'px','left':'initial','transition': '0.3s'});
				
			}
			if($(this).hasClass('return_category_page')){
				$(this).closest('section').removeClass('active');
				$(this).closest('section').css({'right':'-100%','left':'initial','transition': '0.3s'});
				$(this).closest('.voucher_header').css({'right':'-100%','left':'initial','transition': '0.3s'});
				$(this).closest('section').find('.voucher_footer').css({'right':'-100%','left':'initial','transition': '0.3s'});
				
				$('.category_page').addClass('active');
				$('.category_page').css({'right':+w+'px','left':'initial','transition': '0.3s'});
				$('.category_page .voucher_header').css({'right':+w+'px','left':'initial','transition': '0.3s'});
				$('.category_page .voucher_footer').css({'right':+w+'px','left':'initial','transition': '0.3s'});
				
			}
			if($(this).hasClass('return_brand_page')){
				$(this).closest('section').removeClass('active');
				$(this).closest('section').css({'right':'-100%','left':'initial','transition': '0.3s'});
				$(this).closest('.voucher_header').css({'right':'-100%','left':'initial','transition': '0.3s'});
				$(this).closest('section').find('.voucher_footer').css({'right':'-100%','left':'initial','transition': '0.3s'});
				
				$('.brand_page').addClass('active');
				$('.brand_page').css({'right':+w+'px','left':'initial','transition': '0.3s'});
				$('.brand_page .voucher_header').css({'right':+w+'px','left':'initial','transition': '0.3s'});
				$('.brand_page .voucher_footer').css({'right':+w+'px','left':'initial','transition': '0.3s'});
				
			}
			$('.wrap_filter').removeClass('active');
			$('.all_location_btn').removeClass('open');
		});

		$('body').on('click','.return_voucher_btn',function(e){
			e.preventDefault();
			if($(window).width() > 414){
	            var w = ($(window).width() - $("#voucherWrapper").width())/2;
	        }
	        else{
	            var w = 0;
	        }
	        $('.voucher_page').addClass('active');
			$('.voucher_page').css({'right':+w+'px','left':'initial','transition': '0.3s'});
			$('.voucher_page .voucher_header').css({'right':+w+'px','left':'initial','transition': '0.3s'});
			$('.voucher_page .voucher_footer').css({'right':+w+'px','left':'initial','transition': '0.3s'});
			
			//save page left to right
			$('.save_voucher_page').removeClass('active');
			$('.save_voucher_page').css({'right':'-100%','left':'initial','transition': '0.3s'});
			$('.save_voucher_page .voucher_header').css({'right':'-100%','left':'initial','transition': '0.3s'});
			$('.save_voucher_page .voucher_footer').css({'right':'-100%','left':'initial','transition': '0.3s'});
			

			//save page left to right
			$('.category_page').removeClass('active');
			$('.category_page').css({'right':'-100%','left':'initial','transition': '0.3s'});
			$('.category_page .voucher_header').css({'right':'-100%','left':'initial','transition': '0.3s'});
			$('.category_page .voucher_footer').css({'right':'-100%','left':'initial','transition': '0.3s'});
			
			//brand page left to right
			$('.brand_page').removeClass('active');
			$('.brand_page').css({'right':'-100%','left':'initial','transition': '0.3s'});
			$('.brand_page .voucher_header').css({'right':'-100%','left':'initial','transition': '0.3s'});
			$('.brand_page .voucher_footer').css({'right':'-100%','left':'initial','transition': '0.3s'});
			
			//location page left to right
			$('.location_page').removeClass('active');
			$('.location_page').css({'right':'-100%','left':'initial','transition': '0.3s'});
			$('.location_page .voucher_header').css({'right':'-100%','left':'initial','transition': '0.3s'});
			$('.location_page .voucher_footer').css({'right':'-100%','left':'initial','transition': '0.3s'});
			
			$('input[name="brand_id_choose"]').val('');
			$('input[name="cate_selected"]').val('');
			$('.all_location_btn').text("<?php echo trans('content.all_location')?>");
			$('.accordion-1').closest('.accordion-section').find('a').text('<?php echo trans("content.all_city")?>');
			$('.accordion-2').closest('.accordion-section').find('a').text('<?php echo trans("content.choose_district")?>');
		});

		$('body').on('click','.category_item a',function(e){
			e.preventDefault();
			$('.loading').css({'display':'block'});
			var id = $(this).data().id;
			var total = $(this).data().total;
			var cate_name = $(this).data().name;
			var lang = $('html').attr('lang');
			var store_group_id = $('input[name="store_group_id"]').val();
			$('input[name="cate_selected"]').val(id);
			$('.all_location_btn').text("<?php echo trans('content.all_location')?>");
			$('a[href="#accordion-1"]').text("<?php echo trans('content.all_city')?>");
			$('a[href="#accordion-2"]').text('<?php echo trans("content.choose_district")?>');
			$('.accordion-section ul.district li').removeClass('active');
			$.ajax({
				url:'/voucher/getbrand',
				method:'POST',
				data:{cate_id:id,store_group_id:store_group_id},
				// beforeSend: function() { 
				//     $('.loading').css({'display':'block'});
				// }, //Show spinner
				// complete: function() {
				// 	$('.loading').css({'display':'none'});
				// },
				success:function(response){
					//$('.category_page').hide();
					$('.brand_list .wrap_list').html('');
					var html = '';
					if(response['list_brand'].length > 0){
						$('.wrap_list').removeClass('no-data');
						$.each(response['list_brand'],function(index,brand){
							var new_brand = false;
							var brand_name = brand.brand_name_vi;
							if(lang != 'vi'){
		                        brand_name = brand.brand_name_en;
		                    }
							html += '<div class="brand_item">'+
										'<a href="javascript:void(0)" data-id="'+brand.brand_id+'" data-name="'+brand_name+'">'+
										'<img class="logo-brand" src="https://img.gotit.vn/'+brand.logo_image+'">'+
										'</a>'+
										(new Date(brand.brand_created_at) > new Date().setDate(new Date().getDate()- parseInt("<?php echo env('SET_DATE_NEWBRAND')?>")) ? '<span class="new_br_ic"></span>':'')+
									'</div>';
						});
					}
					else{
						$('.wrap_list').addClass('no-data');
						html = '<p style="text-align:center"><?php echo trans("content.no_data")?></p>';
					}
					
					//add city in dropdown filter
					$('ul.city').empty();
					$.each(response['list_city'],function(index,city){
	                    if(lang == 'vi'){
	                        $("ul.city").append('<li id="'+city.city_id+'">'+city.city_name_vi+'</li>');
	                    }
	                    else{
	                        $("ul.city").append('<li id="'+city.city_id+'">'+city.city_name_en+'</li>');
	                    }

	                });

					$('.brand_list .wrap_list').append(html);
					$('.brand_page .voucher_body .left p.category').text('').append(cate_name+'<span>('+total+')</span>');
					//$('.brand_page').show();
					if($(window).width() > 414){
			            var w = ($(window).width() - $("#voucherWrapper").width())/2;
			        }
			        else{
			            var w = 0;
			        }
					//brand page left to right
					$('.brand_page').addClass('active');
					$('.brand_page').css({'right':+w+'px','left':'initial','transition':'0.3s'});
					$('.brand_page .voucher_header').css({'right':+w+'px','left':'initial','transition':'0.3s'});
					$('.brand_page .voucher_footer').css({'right':+w+'px','left':'initial','transition':'0.3s'});
					
					//category page left to right
					$('.category_page').removeClass('active');
					$('.category_page').css({'right':'100%','left':'initial','transition':'0.3s'});
					$('.category_page .voucher_header').css({'right':'100%','left':'initial','transition':'0.3s'});
					$('.category_page .voucher_footer').css({'right':'100%','left':'initial','transition':'0.3s'});

				},
			}).done(function(){
	            $('.loading').css({'display':'none'});
	        });
		});


		// $('body').on('click','.voucher_page .save_btn',function(e){
		// 	e.preventDefault();
		// 	var lang = $('html').attr('lang');
		// 	var code = $(this).data().code;
		// 	$.ajax({
		// 		url:'/save-new-voucher/'+lang+'/'+code,
		// 		type:'get',
		// 		dataType:'json',
				
		// 		success:function(response){
		// 			$('.save_voucher_page .content_body img.img_vc').attr('src',response);
		// 			// $('.save_voucher_page').show();
		// 			// $('.voucher_page').hide();
		// 			if($(window).width() > 414){
		// 	            var w = ($(window).width() - $("#voucherWrapper").width())/2;
		// 	        }
		// 	        else{
		// 	            var w = 0;
		// 	        }
		// 	        $('.save_voucher_page').addClass('active');
		// 			$('.save_voucher_page').css({'right':+w+'px','left':'initial','transition': '0.3s'});
		// 			$('.save_voucher_page .voucher_header').css({'right':+w+'px','left':'initial','transition': '0.3s'});
		// 			$('.save_voucher_page .voucher_footer').css({'right':+w+'px','left':'initial','transition': '0.3s'});
					
		// 			$('.voucher_page').removeClass('active');
		// 	        $('.voucher_page').css({'right':'100%','left':'initial','transition':'0.3s'});
		// 	        $('.voucher_page .voucher_header').css({'right':'100%','left':'initial','transition':'0.3s'});
		// 	        $('.voucher_page .voucher_footer').css({'right':'100%','left':'initial','transition':'0.3s'});
			        
		// 		}
		// 	})
		// });
		
		$('body').on('click','.where_use_btn.vc_multi',function(e){
	        e.preventDefault();
	        if($(window).width() > 414){
	            var w = ($(window).width() - $("#voucherWrapper").width())/2;
	        }
	        else{
	            var w = 0;
	        }
	        // $('.voucher_page').hide();
	        // $('.category_page').show();
	        $('.category_page').addClass('active');
	        $('.category_page').css({'right':+w+'px','left':'initial','transition':'0.3s'});
			$('.category_page .voucher_header').css({'right':+w+'px','left':'initial','transition':'0.3s'});
			$('.category_page .voucher_footer').css({'right':+w+'px','left':'initial','transition':'0.3s'});
	        
			$('.voucher_page').removeClass('active');
	        $('.voucher_page').css({'right':'100%','left':'initial','transition':'0.3s'});
	        $('.voucher_page .voucher_header').css({'right':'100%','left':'initial','transition':'0.3s'});
	        $('.voucher_page .voucher_footer').css({'right':'100%','left':'initial','transition':'0.3s'});
	        
	    });

		var fixed = document.getElementById('mySidenav');

	    fixed.addEventListener('touchmove', function(e) {

	        e.preventDefault();

	    }, false);
		$('body').on({
	        'click touchstart': function (e) {

	            if ($('.sidenav').hasClass('showMenu') &&!$(e.target).closest('.sidenav').length) {
	                e.preventDefault();
	                $('.menu_icon').trigger('click');
	            }
	        }
	        ,
	        keyup: function (e) {
	            if (e.keyCode == 27 && $('.sidenav').hasClass('showMenu')) {
	                $('.menu_icon').trigger('click');
	            }
	        }
	    });


	});

	</script>
	<script type="text/javascript">
	    <?php
	    $googleMaps = array();
	    foreach ($stores as $store) {
	        $googleMaps[] = array(
	            'name'  => Translate::transObj($store, 'name'),
	            'brand_name'  => Translate::transObj($store, 'brand_name'),
	            'lat'   => $store->lat,
	            'lng'   => $store->long,
	            'address'   =>  Translate::transObj($store, 'address'),
	            'phone' => $store->phone
	        );
	    }
	    echo 'var mapStores = '.json_encode($googleMaps);
	    ?>
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
	<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjbeyHo86kWXc3nDZYK8q4AW5Joi0mvOY&v=3.exp&libraries=places"></script> -->
	<script type="text/javascript">
	$(function(){
		// if($('.step_one.welcome_page').hasClass('active')){
		// 	$('#mySidenav').css({'display':'none'});
		// }
		// else{
		// 	$('#mySidenav').css({'display':'block'});
		// }

	    $('.off_store_link').click(function(){
	    	$('.use_store').css({'display':'block'});
	    	$('.code_section').css({'display':'block'});
	    	$('.phontopup').css({'display':'none'});
	    	$('.use_topup').css({'display':'none'});
	    	$('.voucher_save').css({'display':'block'});
	    	$('.voucher_page .voucher_footer').css({'display':'block'});
	    });
	    $('.topup_card_link').click(function(){
	    	$('.use_store').css({'display':'none'});
	    	$('.code_section').css({'display':'none'});
	    	$('.use_topup').css({'display':'block'});
	    	$('.phontopup').css({'display':'block'});
	    	//remove save voucher
	    	$('.voucher_save').css({'display':'none'});
	    	$('.voucher_page .voucher_footer').css({'display':'none'});
	    });

	    $('body').on('click','.where_use_btn.vc_normal',function(e){
	        e.preventDefault();
	        var brand_id = $(this).data().brand;
	        $('input[name="brand_id_choose"]').val(brand_id);
	        var store_group_id = $("input[name='store_group_id']").val();
	        var city_id = $('ul.city li.active').attr('id');
	        var dist_id = $('ul.district li.active').attr('id');
	        $.ajax({
	            url:'/voucher/getCityByBrand',
	            type:'POST',
	            data:{brand_id:brand_id},
	            dataType:'json',
	            success:function(response){
	               //add city in dropdown filter
	                $('ul.city').empty();
	                $.each(response['list_city'],function(index,city){
	                    if(lang == 'vi'){
	                        $("ul.city").append('<li id="'+city.city_id+'">'+city.city_name_vi+'</li>');
	                    }
	                    else{
	                        $("ul.city").append('<li id="'+city.city_id+'">'+city.city_name_en+'</li>');
	                    }

	                }); 
	            }
	        })

	        $.ajax({
	            url: "/voucher/getstore",
	            type: 'POST',
	            data: {brand_id: brand_id,store_group_id:store_group_id,city_id:city_id,dist_id:dist_id},
	            dataType: 'json',
	            success: function(data){
	                $("input[name='brand_id_choose']").val(brand_id);
	                mapStores.length = 0;
	                $(".list-add ul").empty();
	                var number = 0;
	                var browser = navigator.userAgent;
	               
	                $.each(data,function(index, storeObj){
	                        var store_name = storeObj.name_vi;
	                        var store_address = storeObj.address_vi;
	                        var brand_name = storeObj.brand_name_vi;
	                        var phoneText = "Số điện thoại: ";
	      
	                    if(lang != 'vi'){
	                        store_name = storeObj.name_en;
	                        store_address = storeObj.address_en;
	                        brand_name = storeObj.brand_name_en;
	                        phoneText = "Phone: ";
	                    }

	                    if(browser.match(/(iPhone|iPod|iPad)/)){
	                        var link = 'comgooglemaps://?daddr='+store_address+'&directionsmode=driving';
	                    } 
	                    else if(browser.match(/Android/)){
	                        var link ='google.navigation:q='+store_address+'&'+storeObj.lat+','+storeObj.long;
	                    }

	                    mapStores[number] = {'name': store_name, 'lat': storeObj.lat, 'lng': storeObj.long, 'address': store_address, 'brand_name':brand_name};
	                    $(".list-add ul").append('<li><p class="goto-location" data-number="'+ number +'" style="cursor: pointer;">'
	                        + brand_name + ' - ' + store_name +'<br/><span>' + store_address + '</span><br><span>'+phoneText+storeObj.phone+'</span><a href="'+link+'"><img src="../layouts/v2/images/new_voucher/direct.png"></a></p></li>'
	                    );
	                    number = number + 1;

	                });
	                // $('.location_page').show();
	                // $('.brand_page').hide();
	            }
	        }).done(function(){
	            var lat;
	            var long;
	            
	            googleMap._initLat = lat;
	            googleMap._initLng = long;
	            googleMap.init();
	        });
	        // $('.voucher_page').hide();
	        // $('.location_page').show();
	        if($(window).width() > 414){
	            var w = ($(window).width() - $("#voucherWrapper").width())/2;
	        }
	        else{
	            var w = 0;
	        }
	        // $('.voucher_page').hide();
	        // $('.category_page').show();
	        $('.location_page').addClass('active');
	        $('.location_page').css({'right':+w+'px','left':'initial','transition':'0.3s'});
			$('.location_page .voucher_header').css({'right':+w+'px','left':'initial','transition':'0.3s'});
			$('.location_page .voucher_footer').css({'right':+w+'px','left':'initial','transition':'0.3s'});
	        
			$('.voucher_page').removeClass('active');
	        $('.voucher_page').css({'right':'100%','left':'initial','transition':'0.3s'});
	        $('.voucher_page .voucher_header').css({'right':'100%','left':'initial','transition':'0.3s'});
	        $('.voucher_page .voucher_footer').css({'right':'100%','left':'initial','transition':'0.3s'});
	    });
	    $("body").on('click','.city li',function(e){
	        //e.preventDefault();
	        category_id = $('input[name="cate_selected"]').val();
	        var lang = $("html").attr('lang');
	        if(lang == 'vi'){
	            var dist = 'Vui lòng chọn Quận/Huyện';
	        }
	        else{
	            var dist = 'Please choose District';
	        }
	        if(!$(this).hasClass('active')){
	            $('.accordion-2').closest('.accordion-section').find('a').text(dist);
	        }
	        var str = $(this).text();
	        $(".city li").removeClass('active');
	        $(this).addClass('active');
	        $( "a.accordion-section-title.active" ).text( str );
	        $( "a.accordion-section-title.active" ).css({'color':'#424242'});
	        close_accordion_section();

	        city_id = $(this).attr("id");
	        //setDefaultMap(city_id);

	        var url = 'product';
	        var brand_id = $("input[name='brand_id_choose']").val();
	        var store_group_id = $("input[name='store_group_id']").val();

	        if(typeof store_group_id == 'undefined' || store_group_id.length == 0){
	            store_group_id = 0;
	        }

	        $.ajax({
	            url: "/voucher/getDistrictCity",
	            data: {category_id:category_id,city_id: city_id,brand_id:brand_id,store_group_id:store_group_id},
	            type:'POST',
	            dataType: 'json',
	            success: function(data){
	                $("ul.district").empty();
	                $.each(data['list_district'],function(index,districtObj){
	                    if(lang == 'vi'){
	                        $("ul.district").append('<li id="'+districtObj.dist_id+'">'+districtObj.name_vi+'</li>');
	                    }
	                    else{
	                        $("ul.district").append('<li id="'+districtObj.dist_id+'">'+districtObj.name_en+'</li>');
	                    }

	                });
	            }
	        });
	    });

	    $("body").on('click','.district li',function(e){
	        //e.preventDefault(e);
	        var lang = $("html").attr('lang');
	        var str = $(this).text();
	        $(".district li").removeClass('active');
	        $(this).addClass('active');
	        $( "a.accordion-section-title.active" ).text( str );
	        $( "a.accordion-section-title.active" ).css({'color':'#424242'});
	        close_accordion_section();

	        dist_id = $(this).attr("id");
	        var url = $("#url_get_data").val();
	        var brand = $("#brand").val();
	        var store_group_id = $("input[name='store_group_id']").val();

	        if(typeof store_group_id == 'undefined' || store_group_id.length == 0){
	            store_group_id = 0;
	        }
	    });


	//apply btn in brand page show brand
	    $('body').on('click','.brand_page .apply_filter_btn',function(e){
	        e.preventDefault(e);
	        var lang = $('html').attr('lang');
	        var cate_id = $('input[name="cate_selected"]').val();
	        var city_id = $('.brand_page ul.city li.active').attr('id');
	        var dist_id = $('.brand_page ul.district li.active').attr('id');
	        if(typeof city_id == 'undefined'){
	            city_id = 0;
	        }
	        if(typeof dist_id == 'undefined'){
	            dist_id = 0;
	        }

	        $.ajax({
	            url:'/voucher/getbrand',
	            method:'POST',
	            data:{cate_id:cate_id,city_id:city_id,dist_id:dist_id},
	            success:function(response){
	                $('.brand_list .wrap_list').html('');
	                var html = '';
	                if(response['list_brand'].length > 0){
	                	$('.brand_list .wrap_list').removeClass('no-data');
	                	$.each(response['list_brand'],function(index,brand){
		                    var brand_name = brand.brand_name_vi;
		                    if(lang != 'vi'){
		                        brand_name = brand.brand_name_en;
		                    }
		                    html += '<div class="brand_item">'+
		                                '<a href="javascript:void(0)" data-id="'+brand.brand_id+'" data-name="'+brand_name+'"><img class="logo-brand" src="https://img.gotit.vn/'+brand.logo_image+'"></a>'+
		                                (new Date(brand.brand_created_at) > new Date().setDate(new Date().getDate()- parseInt("<?php echo env('SET_DATE_NEWBRAND')?>")) ? '<span class="new_br_ic"></span>':'')+
		                            '</div>';
		                });
	                }
	                else{
	                	$('.brand_list .wrap_list').addClass('no-data');
	                	html = '<p style="text-align:center"><?php echo trans("content.no_data")?></p>';
	                }
	                
	                $('.brand_list .wrap_list').append(html);

	                var city_select = $('ul.city li.active').text();
	                var dist_select = $('ul.district li.active').text();
	                if(typeof city_select != 'undefined' && city_select != ''){
		                if(typeof dist_select != 'undefined' && dist_select != ''){
		                    $('.all_location_btn').text(city_select +' - '+dist_select);
		                }
		                else{
		                	$('.all_location_btn').text(city_select);
		                }
		            }
	                else{
	                    $('.all_location_btn').text("<?php echo trans('content.all_location')?>");
	                }
	                
	                $('.all_location_btn').removeClass('open');
	                $('.wrap_filter').removeClass('active');
	            },
	        });
	    });

	$(window).resize(function() {
	  	if($(window).width() > 414){
	        var w = ($(window).width() - $("#voucherWrapper").width())/2;
	        $('.menu_icon').css({'left':-(w+42)+'px'});
	    }
	    else{
	        var w = 0;
	        $('.menu_icon').css({'left':'-42px'});
	    }
	    $('#voucherWrapper.active').css({'right':w,'left':'inherit'});
	    $('#voucherWrapper.active .voucher_header').css({'right':w,'left':'inherit'});
	    $('#voucherWrapper.active .voucher_footer').css({'right':w,'left':'inherit'});
	    
	    var img_height = $('img.netis_img').height();
	    var height_wd = $(window).height() - 60;   // returns height of browser viewport
	    console.log(height_wd + 60);
	    if(height_wd <= img_height){
			$('img.netis_img').css({'height':height_wd,'display':'block'});
		}
		else{
			$('img.netis_img').css({'height':'auto','display':'block'});
		}
	});

	//apply btn in location page show store
	    $('body').on('click','.location_page .apply_filter_btn',function(e){
	        e.preventDefault(e);
	        var lang = $('html').attr('lang');
	        var brand_id = $('input[name="brand_id_choose"]').val();
	        var store_group_id = $("input[name='store_group_id']").val();
	        var city_id = $('ul.city li.active').attr('id');
	        var dist_id = $('ul.district li.active').attr('id');
	        if(typeof city_id == 'undefined'){
	            city_id = 0;
	        }
	        if(typeof dist_id == 'undefined'){
	            dist_id = 0;
	        }
	        $.ajax({
	            url: "/voucher/getstore",
	            type: 'POST',
	            data: {brand_id: brand_id,store_group_id:store_group_id,city_id:city_id,dist_id:dist_id},
	            dataType: 'json',
	            success: function(data){
	                mapStores.length = 0;
	                $(".list-add ul").empty();
	                var number = 0;
	                var browser = navigator.userAgent;
	               	if(data.length > 0){
	               		$.each(data,function(index, storeObj){
	                        var store_name = storeObj.name_vi;
	                        var store_address = storeObj.address_vi;
	                        var brand_name = storeObj.brand_name_vi;
	                        var phoneText = "Số điện thoại: ";
	      
		                    if(lang != 'vi'){
		                        store_name = storeObj.name_en;
		                        store_address = storeObj.address_en;
		                        brand_name = storeObj.brand_name_en;
		                        phoneText = "Phone: ";
		                    }

		                    if(browser.match(/(iPhone|iPod|iPad)/)){
		                        var link = 'comgooglemaps://?daddr='+store_address+'&directionsmode=driving';
		                    } 
		                    else if(browser.match(/Android/)){
		                        var link ='google.navigation:q='+store_address+'&'+storeObj.lat+','+storeObj.long;
		                    }

		                    mapStores[number] = {'name': store_name, 'lat': storeObj.lat, 'lng': storeObj.long, 'address': store_address, 'brand_name':brand_name};
		                    $(".list-add ul").append('<li><p class="goto-location" data-number="'+ number +'" style="cursor: pointer;">'
		                        + brand_name + ' - ' + store_name +'<br/><span>' + store_address + '</span><br><span>'+phoneText+storeObj.phone+'</span><a href="'+link+'"><img src="../layouts/v2/images/new_voucher/direct.png"></a></p></li>'
		                    );
		                    number = number + 1;

		                });
	               	}
	               	else{

	               	}
	                
	                

	                var city_select = $('.location_page ul.city li.active').text();
	                var dist_select = $('.location_page ul.district li.active').text();
	             
	                if(typeof dist_select != 'undefined' && dist_select != ''){
	                    $('.all_location_btn').text(city_select +' - '+dist_select);
	                }
	                else{
	                    $('.all_location_btn').text(city_select);
	                }
	                
	                $('.all_location_btn').removeClass('open');
	                $('.wrap_filter').removeClass('active');

	                // $('.location_page').show();
	                // $('.brand_page').hide();
	                
	                if($(window).width() > 414){
			            var w = ($(window).width() - $("#voucherWrapper").width())/2;
			        }
			        else{
			            var w = 0;
			        }
			        $('.location_page').addClass('active');
					$('.location_page').css({'right':+w+'px','left':'initial','transition': '0.3s'});
					$('.location_page .voucher_header').css({'right':+w+'px','left':'initial','transition': '0.3s'});
					$('.location_page .voucher_footer').css({'right':+w+'px','left':'initial','transition': '0.3s'});
					
					$('.brand_page').removeClass('active');
			        $('.brand_page').css({'right':'100%','left':'initial','transition':'0.3s'});
			        $('.brand_page .voucher_header').css({'right':'100%','left':'initial','transition':'0.3s'});
			        $('.brand_page .voucher_footer').css({'right':'100%','left':'initial','transition':'0.3s'});
			        
	            }
	        }).done(function(){
	            var lat;
	            var long;
	            
	            googleMap._initLat = lat;
	            googleMap._initLng = long;
	            googleMap.init();

	            $('.map-wrap .map').css({"z-index":"99","max-width":"375px"});
	            if($(window).width() > 414){
	                var right = ($(window).width() - $("#voucher_wrapper").width())/2;
	            }
	            else{
	                var right = 0;
	            }
	            $('.map-wrap .map').removeClass('active').animate({
	                'right' : right,
	            });
	        });
	    });

	    $(document).on("click",".goto-location", function(){
	        if($('.list-add ul li').hasClass('active')){
	            $('.list-add ul li').removeClass('active');
	        }
	        $(this).parent().addClass('active');

	        
	    });

	    $('.list .wrap_img img').bind('touchstart', function(e) {
	        $(this).addClass('hover_effect');
	        $('.all_brand_n .list .wrap_img').slick('slickPause');
	        //e.preventDefault();
	    });
	    $('.list .wrap_img img').bind('touchend', function(e) {
	        $(this).removeClass('hover_effect');
	        $('.all_brand_n .list .wrap_img').slick('slickPlay');
	        //e.preventDefault();
	    });
	 

	    var img_server = $("input[name='img_server']").val();

	    
	    $('body').on('click','.brand_item a',function(e){
	        e.preventDefault();
	        $('.loading').css({'display':'block'});
	        var brand_name = $(this).data().name;
	        var brand_id = $(this).data().id;
	        //var url = $("#url_get_data").val();
	        var store_group_id = $("input[name='store_group_id']").val();
	        var city_id = $('.brand_page ul.city li.active').attr('id');
	        var city_text = $('.brand_page ul.city li.active').text();
	        var dist_id = $('.brand_page ul.district li.active').attr('id');
	        var dist_text = $('.brand_page ul.district li.active').text();
	        var lang = $('html').attr('lang');

	        if(typeof store_group_id == 'undefined' || store_group_id.length == 0){
	            store_group_id = 0;
	        }
	        $.ajax({
	            url:'/voucher/getCityByBrand',
	            type:'POST',
	            data:{brand_id:brand_id},
	            dataType:'json',
	   //          beforeSend: function() { 
				//     $('.loading').css({'display':'block'});
				// }, //Show spinner
				// complete: function() {
				// 	$('.loading').css({'display':'none'});
				// },
	            success:function(response){
	               //add city in dropdown filter
	                $('ul.city').empty();
	                $.each(response['list_city'],function(index,city){
	  
	                    if(lang == 'vi'){
	                        $("ul.city").append('<li id="'+city.city_id+'" class="'+(city_id == city.city_id ? 'active':'')+'">'+city.city_name_vi+'</li>');
	                    }
	                    else{
	                        $("ul.city").append('<li id="'+city.city_id+'" class="'+(city_id == city.city_id ? 'active':'')+'">'+city.city_name_en+'</li>');
	                    }

	                }); 
	            }
	        });

	        $.ajax({
	            url: "/voucher/getDistrictCity",
	            data: {city_id: city_id,brand_id:brand_id,store_group_id:store_group_id},
	            type:'POST',
	            dataType: 'json',
	            success: function(data){
	                $("ul.district").empty();
	                $.each(data['list_district'],function(index,districtObj){
	                    if(lang == 'vi'){
	                        $("ul.district").append('<li id="'+districtObj.dist_id+'" class="'+(districtObj.dist_id == dist_id ? 'active':'')+'">'+districtObj.name_vi+'</li>');
	                    }
	                    else{
	                        $("ul.district").append('<li id="'+districtObj.dist_id+'" class="'+(districtObj.dist_id == dist_id ? 'active':'')+'">'+districtObj.name_en+'</li>');
	                    }

	                });
	            }
	        });

	        if(typeof city_id != undefined && city_id > 0){
	        	$('.accordion-1').closest('.accordion-section').find('a').text(city_text);
	        }
	        if(typeof dist_id != undefined && dist_id > 0){
	        	$('.accordion-2').closest('.accordion-section').find('a').text(dist_text);
	        }
	        


	        $.ajax({
	            url: "/voucher/getstore",
	            type: 'POST',
	            data: {brand_id: brand_id,store_group_id:store_group_id,city_id:city_id,dist_id:dist_id},
	            dataType: 'json',
	            success: function(data){
	                $("input[name='brand_id_choose']").val(brand_id);
	                mapStores.length = 0;
	                $(".list-add ul").empty();
	                var number = 0;
	                var browser = navigator.userAgent;
	               
	                $.each(data,function(index, storeObj){
	                        var store_name = storeObj.name_vi;
	                        var store_address = storeObj.address_vi;
	                        var brand_name = storeObj.brand_name_vi;
	                        var phoneText = "Số điện thoại: ";
	      
	                    if(lang != 'vi'){
	                        store_name = storeObj.name_en;
	                        store_address = storeObj.address_en;
	                        brand_name = storeObj.brand_name_en;
	                        phoneText = "Phone: ";
	                    }

	                    if(browser.match(/(iPhone|iPod|iPad)/)){
	                        var link = 'comgooglemaps://?daddr='+store_address+'&directionsmode=driving';
	                    } 
	                    else if(browser.match(/Android/)){
	                        var link ='google.navigation:q='+store_address+'&'+storeObj.lat+','+storeObj.long;
	                    }

	                    mapStores[number] = {'name': store_name, 'lat': storeObj.lat, 'lng': storeObj.long, 'address': store_address, 'brand_name':brand_name};
	                    $(".list-add ul").append('<li><p class="goto-location" data-number="'+ number +'" style="cursor: pointer;">'
	                        + brand_name + ' - ' + store_name +'<br/><span>' + store_address + '</span><br><span>'+phoneText+storeObj.phone+'</span><a href="'+link+'"><img src="../layouts/v2/images/new_voucher/direct.png"></a></p></li>'
	                    );
	                    number = number + 1;

	                });
	                $('.location_page .voucher_body .top_body p.brand_name').text(brand_name);
	                // $('.location_page').show();
	                // $('.brand_page').hide();
	                if($(window).width() > 414){
			            var w = ($(window).width() - $("#voucherWrapper").width())/2;
			        }
			        else{
			            var w = 0;
			        }
			        $('.location_page').addClass('active');
					$('.location_page').css({'right':+w+'px','left':'initial','transition': '0.3s'});
					$('.location_page .voucher_header').css({'right':+w+'px','left':'initial','transition': '0.3s'});
					$('.location_page .voucher_footer').css({'right':+w+'px','left':'initial','transition': '0.3s'});
					
					$('.brand_page').removeClass('active');
			        $('.brand_page').css({'right':'100%','left':'initial','transition':'0.3s'});
			        $('.brand_page .voucher_header').css({'right':'100%','left':'initial','transition':'0.3s'});
			        $('.brand_page .voucher_footer').css({'right':'100%','left':'initial','transition':'0.3s'});
			        

	            }
	        }).done(function(){
	            var lat;
	            var long;
	            
	            googleMap._initLat = lat;
	            googleMap._initLng = long;
	            googleMap.init();

	            $('.loading').css({'display':'none'});
	        });

	        

	    });


	 var locations = [];
	    
	    $(".list-add a.find_near").click(function(e){
	        e.preventDefault(e);
	        var brand_id = $(this).data().brandid;
	        var store_group_id = $(this).data().storegroupid;
	        var brand_choose = $("input[name='brand_id_choose']").val();
	        var city_id = 0;
	        var district = 0;
	        var lang = $('html').attr('lang');

	        if(typeof store_group_id == 'undefined' || store_group_id.length == 0){
	            store_group_id = 0;
	        }
	        if(typeof brand_choose == 'undefined' || brand_choose.length == 0){
	            brand_choose = 0;
	        }

	        $.ajax({
	            url: "/findnearstore/",
	            type: 'POST',
	            data: {brand_id: brand_id, city_id: city_id,store_group_id:store_group_id,brand_choose:brand_choose},
	            //data: {brand_id: brand_id,store_group_id:store_group_id},
	            dataType: 'json',
	            success: function(data){
	                locations = data;
	                FindNear();
	            }
	        }).done(function(data){
	            var lat;
	            var long;
	        });
	        
	    });

	    var map;    // Google map object

	    // Initialize and display a google map
	    function FindNear()
	    {
	        // HTML5/W3C Geolocation
	        if ( navigator.geolocation )
	        {
	            navigator.geolocation.getCurrentPosition( UserLocation, errorCallback,{maximumAge:60000,enableHighAccuracy: true,timeout:10000});
	        }
	        
	        else
	            ClosestLocation( 10.767661, 106.6999359, "254 Nguyen Cong Tru" );
	    }

	    function errorCallback( error )
	    {
	    }
	    // Callback function for asynchronous call to HTML5 geolocation
	    function UserLocation( position )
	    {
	        ClosestLocation( position.coords.latitude, position.coords.longitude, "This is my Location" );
	    }

	    // Display a map centered at the nearest location with a marker and InfoWindow.
	    function ClosestLocation( lat, long, title )
	    {
	        // Create a Google coordinate object for where to center the map
	        var latlng = new google.maps.LatLng( lat, long );    

	        // Map options for how to display the Google map
	        var mapOptions = { zoom: 12, center: latlng  };

	        // Show the Google map in the div with the attribute id 'map-canvas'.
	        map = new google.maps.Map(document.getElementById('embed-map'), mapOptions);

	        // find the closest location to the user's location
	        var closest = 0;
	        var mindist = 99999;

	        for(var i = 0; i < locations.length; i++) 
	        {
	            // get the distance between user's location and this point
	            var dist = Haversine( locations[ i ].lat, locations[ i ].long, lat, long );
	            locations[i]['distance'] = dist;

	            // check if this is the shortest distance so far
	           
	            // if ( dist < mindist )
	            // {
	            //     closest = i;
	            //     mindist = dist;
	            // }

	        }

	        locations.sort(function(a,b) { 
	            return parseFloat(a.distance) - parseFloat(b.distance) ;
	        });
	     
	        // Create a Google coordinate object for the closest location
	        var latlng = new google.maps.LatLng( locations[ closest].lat, locations[ closest].long );    
	        mapStores.length = 0;

	        $(".list-add ul").empty();
	        var number = 0;
	        var browser = navigator.userAgent;
	        var lang = $('html').attr('lang');       

	        $.each(locations,function(index, storeObj){
	            var store_name = storeObj.name_vi;
	            var store_address = storeObj.address_vi;
	            var brand_name = storeObj.brand_name_vi;
	            var phoneText = "Số điện thoại: ";

	            if(lang != 'vi'){
	                store_name = storeObj.name_en;
	                store_address = storeObj.address_en;
	                brand_name = storeObj.brand_name_en;
	                phoneText = "Phone: ";
	            }

	            if(browser.match(/(iPhone|iPod|iPad)/)){
	                var link = 'comgooglemaps://?daddr='+store_address+'&directionsmode=driving';
	            } 
	            else if(browser.match(/Android/)){
	                var link ='google.navigation:q='+store_address+'&'+storeObj.lat+','+storeObj.long;
	            }

	            //$(".top_map h3").html('<img src="'+img_server+storeObj.img_path+'" alt="">'+brand_name+'');
	            mapStores[number] = {'name': store_name, 'lat': storeObj.lat, 'lng': storeObj.long, 'address': store_address,'brand_name':brand_name};
	            $(".list-add ul").append('<li><p class="goto-location" data-number="'+ number +'" style="cursor: pointer;">'
	                + brand_name + ' - ' + store_name +'<br/><span>' + store_address + '</span><br><span>'+phoneText+storeObj.phone+'</span><a href="'+link+'"><img src="../layouts/v2/images/new_voucher/direct.png"></a></p></li>'
	            );
	            number = number + 1;
	        });

	        googleMap.init();

	        setTimeout(function(){ $(".list-add ul li:first .goto-location").trigger('click'); }, 1000);
	        
	    }

	    // Convert Degress to Radians
	    function Deg2Rad( deg ) {
	       return deg * Math.PI / 180;
	    }

	    // Get Distance between two lat/lng points using the Haversine function
	    // First published by Roger Sinnott in Sky & Telescope magazine in 1984 (“Virtues of the Haversine”)
	    //
	    function Haversine( lat1, long1, lat2, long2 )
	    {
	        var R = 6372.8; // Earth Radius in Kilometers

	        var dLat = Deg2Rad(lat2-lat1);  
	        var dlong = Deg2Rad(long2-long1);  

	        var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(Deg2Rad(lat1)) * Math.cos(Deg2Rad(lat2)) * Math.sin(dlong/2) * Math.sin(dlong/2);  
	        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	        var d = R * c; 

	        // Return Distance in Kilometers
	        return d;
	    }


	    changeLang();
	    googleMap.init();
	   
	});

	function changeLang(){
	    $('.language a').click(function() {
	        var curr_lang = $('html').attr('lang');

	        var lang = $(this).data().lang;

	        if(curr_lang == lang){
	            return;
	        }

	        $.ajax({
	            url: '/change-language',
	            type: 'post',
	            data: {lang: lang},
	            success: function(){
	                location.reload();
	            }
	        });
	    });
	}
	function close_accordion_section() {
	    $('.accordion .accordion-section-title').removeClass('active');
	    $('.accordion .accordion-section-content').slideUp(300).removeClass('open');
	}


	var lang = $('html').attr('lang');
	        var direct_text = 'Chỉ đường';
	        if(lang != 'vi'){
	                direct_text = 'Get directions';
	            }

	//google mapS
	var googleMap = {
	    _gMap: Object,
	    _gMarker: Object,
	    _infoWindow: Object,
	    _arrGMarker: [],
	    _storeList : [],

	    _initLat : '10.7694307' ,
	    _initLng: '106.6854644',

	    init: function () {
	        /*var dataItem = {};
	        dataItem.name = 'Demo';
	        dataItem.lat    = '10.7887048';
	        dataItem.lng    = '106.69275500000003';
	        googleMap._storeList.push(dataItem);*/
	        googleMap._storeList = mapStores;

	        googleMap.initialize();
	        //console.log(googleMap._storeList);
	        //$('.js-btn-gmap').on('click', googleMap.initialize);
	        //$('.js-list-stores .slide').on('click', googleMap.goToMarker);
	       // $('.goto-location').on('click', googleMap.goToMarker);
	        $('.list-add ul').on('click','li .goto-location', googleMap.goToMarker);

	    },
	    initialize: function () {
	       
	        if (typeof google != 'undefined') {


	            var _location = new google.maps.LatLng( googleMap._initLat, googleMap._initLng );
	            var _mapOptions = {center: _location,zoom: 12,scrollwheel: false};
	            googleMap._gMap = new google.maps.Map(document.getElementById("embed-map"), _mapOptions);

	            if (googleMap._storeList.length > 0) {

	                googleMap._infoWindow = new google.maps.InfoWindow();
	                var bounds = new google.maps.LatLngBounds();

	                for (i = 0; i < googleMap._storeList.length; i++) {

	                    var _store = googleMap._storeList[i];
	                    if (_store.lat != '' && _store.lng != '') {
	                        var pos = new google.maps.LatLng(_store.lat, _store.lng);
	                        bounds.extend(pos);

	                        googleMap._gMarker = new google.maps.Marker({ optimized: false, title: _store.name, position: pos, map: googleMap._gMap, icon: _store.image });
	                    

	                        google.maps.event.addListener(googleMap._gMarker, 'click', (function (marker, i) {
	                            return function () {
	                                if (googleMap._gMap.getZoom() < 12) {
	                                    googleMap._gMap.setCenter(marker.getPosition());
	                                }
	                                var browser = navigator.userAgent;
	                                if(browser.match(/(iPhone|iPod|iPad)/)){
	                                    var link = 'comgooglemaps://?daddr='+googleMap._storeList[i].address+'&directionsmode=driving';
	                                } 
	                                else if(browser.match(/Android/)){
	                                    var link ='google.navigation:q='+googleMap._storeList[i].address+'&'+googleMap._storeList[i].lat+','+googleMap._storeList[i].long;
	                                }

	                                googleMap._gMap.setZoom(12);

	                                googleMap._infoWindow.setContent('<div class="wrap_marker">'+
	                                                                    '<h3>' + ((googleMap._storeList[i].brand_name +' - '+ googleMap._storeList[i].name).length > 33 ? (googleMap._storeList[i].brand_name +' - '+ googleMap._storeList[i].name).substr(0,30)+'...' : (googleMap._storeList[i].brand_name +' - '+ googleMap._storeList[i].name)) + '</h3>' + 
	                                                                    
	                                                                    '<p class="address">' + googleMap._storeList[i].address + '</p>'+
	                                                                    '<a href="'+link+'"><p class="get_direction">'+direct_text+'</p></a>'+
	                                                                '</div>');
	                                google.maps.event.addListener(googleMap._infoWindow, 'domready', function() {

	                                    // Reference to the DIV which receives the contents of the infowindow using jQuery
	                                    var iwOuter = $('.gm-style-iw');

	                                    //get parent div
	                                    iwOuter.parent().addClass('m_wrap');
	                                    var iwBackground = iwOuter.prev();

	                                    iwBackground.css({'display':'none'});
	                                    // Remove the background shadow DIV
	                                    iwBackground.children(':nth-child(2)').css({'display' : 'none'});
	                                    // Remove the white background DIV
	                                    iwBackground.children(':nth-child(4)').css({'display' : 'none'});
	                                    // Moves the arrow 76px to the left margin 
	                                    iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

	                                    var close_btn = iwOuter.next().addClass('close_btn');

	                                });
	                                googleMap._infoWindow.open( googleMap._gMap, marker);

	                            }
	                        })(googleMap._gMarker, i));
	                        googleMap._arrGMarker[i] = googleMap._gMarker;
	                    }
	                }

	                googleMap._gMap.fitBounds(bounds);
	                var listener = google.maps.event.addListener(googleMap._gMap, "idle", function () {
	                    if (googleMap._gMap.getZoom() > 12) googleMap._gMap.setZoom(12);
	                    google.maps.event.removeListener(listener);
	                });
	            }
	        }
	    },
	    goToMarker: function (e) {
	        var _iNumber = $(this).data().number;
	        var browser = navigator.userAgent;
	        if(browser.match(/(iPhone|iPod|iPad)/)){
	            var link = 'comgooglemaps://?daddr='+googleMap._storeList[_iNumber].address+'&directionsmode=driving';
	        } 
	        else if(browser.match(/Android/)){
	            var link ='google.navigation:q='+googleMap._storeList[_iNumber].address+'&'+googleMap._storeList[_iNumber].lat+','+googleMap._storeList[_iNumber].long;
	        }
	        var lat = googleMap._storeList[_iNumber].lat;
	        var long = googleMap._storeList[_iNumber].lng;
	        var bounds = new google.maps.LatLngBounds();
	        googleMap._infoWindow.setContent('<div class="wrap_marker">'+
	                                            '<h3>' + ((googleMap._storeList[_iNumber].brand_name +' - '+ googleMap._storeList[_iNumber].name).length > 33 ? (googleMap._storeList[_iNumber].brand_name +' - '+ googleMap._storeList[_iNumber].name).substr(0,30)+'...' : (googleMap._storeList[_iNumber].brand_name +' - '+ googleMap._storeList[_iNumber].name)) + '</h3>' + 
	                                            '<p class="address">'+ googleMap._storeList[_iNumber].address+'</p>'+
	                                            '<a href="'+link+'"><p class="get_direction">'+direct_text+'</p></a>'+
	                                            '</div>');
	        
	        
	        google.maps.event.addListener(googleMap._infoWindow, 'domready', function() {

	                // Reference to the DIV which receives the contents of the infowindow using jQuery
	                var iwOuter = $('.gm-style-iw');

	                //get parent div
	                iwOuter.parent().addClass('m_wrap');
	                var iwBackground = iwOuter.prev();

	                iwBackground.css({'display':'none'});
	                // Remove the background shadow DIV
	                iwBackground.children(':nth-child(2)').css({'display' : 'none'});
	                // Remove the white background DIV
	                iwBackground.children(':nth-child(4)').css({'display' : 'none'});
	                // Moves the arrow 76px to the left margin 
	                iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

	                var close_btn = iwOuter.next().addClass('close_btn');

	            });

	        googleMap._infoWindow.open(googleMap._gMap, googleMap._arrGMarker[_iNumber]);
	      
	        googleMap._gMap.setZoom(12);
	    
	        googleMap._gMap.setCenter(new google.maps.LatLng(lat, long));

	        $('body').scrollTop(0,500);
	        
	    },
	    geocodeAddress: function(address) {
	        geocoder = new google.maps.Geocoder();
	        //In this case it gets the address from an element on the page, but obviously you  could just pass it to the method instead
	        geocoder.geocode( { 'address': address}, function(results, status) {
	          if (status == google.maps.GeocoderStatus.OK) {
	            //In this case it creates a marker, but you can get the lat and lng from the location.LatLng
	            return results[0].geometry.location;
	          } else {
	            alert("Geocode was not successful for the following reason: " + status);
	          }
	        });

	    }
	}

	function GetLocation() {
	    var geocoder = new google.maps.Geocoder();
	    var address = document.getElementById("txtAddress").value;
	    geocoder.geocode({ 'address': address }, function (results, status) {
	        if (status == google.maps.GeocoderStatus.OK) {
	            var latitude = results[0].geometry.location.lat();
	            var longitude = results[0].geometry.location.lng();
	            alert("Latitude: " + latitude + "\nLongitude: " + longitude);
	        } else {
	            alert("Request failed.")
	        }
	    });
	};

	function goto (url) {
	    window.location = url;
	}

	</script>
	</body>
	</html>