<!DOCTYPE html>
<html lang="{{ Cookie::get('laravel_language','vi') }}">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{!!  csrf_token()  !!}" />
    <meta http-equiv="content-language" content="{!!  Cookie::get('laravel_language','vi')  !!}">
    <title>{{trans('content.gotit_title')}}</title>
    <link rel="shortcut icon" href="/favicon.gif">
    <!-- Bootstrap core CSS -->
    <link href="{!! asset('layouts/v4/css/bootstrap.min.css')  !!}" rel="stylesheet">
    <link href="{!! asset('layouts/v4/css/bootstrap-select.min.css')  !!}" rel="stylesheet">
    <link href="{!! asset('layouts/v4/fonts/stylesheet.css')  !!}" rel="stylesheet">
    <link href="{!! asset('layouts/v4/css/icomoonstyle.css')  !!}" rel="stylesheet">
    <link href="{!! asset('layouts/v4/css/custom.css')  !!}" rel="stylesheet">
    <?php
        $browserLang = Request::server('HTTP_ACCEPT_LANGUAGE');
        $langPos = strpos($browserLang, 'vi');

        if($langPos === false){
            $browserLang = 'en';
        }else{
            $browserLang = 'vi';
        }

        $lang = Cookie::get('laravel_language', $browserLang);
        $ip = $_SERVER['REMOTE_ADDR'];
    ?>
    <style type="text/css">
      section{
        margin-top: 8px;
      }
      .info-brand .list-brand{
        margin-top: 20px;
      }
      .info-brand .list-brand h2.heading{
        font-size: 18px;
      }
      .info-voucher .code-wrap{
        margin-bottom: 16px;
      }
      .info-voucher .code-wrap .logo_gotit{
        margin-bottom: 8px;
      }
      .info-voucher .code-wrap .barcode img{
        height: 45px;
      }
      .info-voucher .code-wrap .barcode{
        margin-bottom: 0;
      }
      .info-brand .list-brand .list-item{
        font-size: 0;
      }
      .info-brand .list-brand .list-item .brand_wr{
        border:1px solid #e3e3e3;
        border-radius: 6px;
        margin-right: 12px;
      }
      .info-brand .list-brand .list-item .brand_wr:last-child{
        margin-right: 12px;
      }
      .info-brand .list-brand .list-item .brand_wr .logo-brand{
        width: 70px;
        height: 70px;

      }
    </style>
  </head>

  <body id="page-top">
    <?php //dd($list_brand_id);?>
    <div class="navbar-top">
      <div class="side-nav-panel-left">
        <a href="#" data-activates="slide-out-left" class="side-nav-left menu_icon">
          <span class=""></span></a>
      </div>
      <!-- site brand  -->
      <div class="site-brand">
        <a href="index.html"><img class="logo-top" src="{{($voucher_tp != null && $voucher_tp->customer_logo != null ? env('IMG_SERVER').env('AWS_VOUCHER_TEMPLATE_FOLDER').'/'.$voucher_tp->customer_logo : '../layouts/v4/img/logo.png')}}"></a>
      </div>
      <!-- end site brand  -->
      <div class="side-nav-panel-right">
        <a href="javascript:void(0)" class="btn-language" data-lang="{{$lang}}" href="javascript:void(0)"><span class="icon-language"></span><small>{{$lang == "vi" ? "en":"vn"}}</small></a>
        <!-- <a href="javascript:void(0)"><span class="icon-language"></span><small>vn</small></a> -->
      </div>
    </div>
    <div class="list_menu">
      <ul>
        <li><a href="https://www.gotit.vn/egift#faqs">{{trans('content.how_use_vc')}}</a></li>
        <li><a href="https://www.gotit.vn/faq.html">{{trans('content.faq_v')}}</a></li>
        <li><a href="https://www.gotit.vn/product.html">{{trans('content.popular_gift_v')}}</a></li>
        <li><a href="https://biz.gotit.vn/">{{trans('content.gotit_for_bussiness')}}<small>www.biz.gotit.vn</small></a></li>
        <li><a href="https://www.gotit.vn/">{{trans('content.what_is_gotit')}}<small>www.gotit.vn</small></a></li>
        <li><a href="https://www.facebook.com/quatangGotIt/">{{trans('content.follow_us')}}<small>facebook.com/gotit.vn</small></a></li>
        <li><a href="https://brand.gotit.vn/">{{trans('content.all_brands')}}<small>brand.gotit.vn</small></a></li>
        
        <li class="hotline_menu">Got It Hotline: <b>1900 5588 20</b></li>
      </ul>
      
      <style type="text/css">
        
      </style>
    </div>
    <section class="info-voucher text-center" id="info-voucher">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 mx-auto wrap">
            <h2 class="section-heading" style="background-color:{{($voucher_tp != null && $voucher_tp->customer_color != null ? $voucher_tp->customer_color : '#ff5f5f')}}">{{$psize}}</h2>
            <p>{!! trans('content.show_code_to_staffv4') !!}</p>
            <div class="code-wrap">
                <img src="../layouts/v4/img/logo.png" alt="Got It" class="logo_gotit">
                <p class="barcode">
                    <img src="<?php echo '/voucher/barcode/'.$voucher->code?>" />
                    <!-- <img src="../layouts/v4/img/barcode.png" alt=""> -->
                    <span class="code">{{$voucher->code}}</span>
                </p>
                <p class="validate">{{ trans('content.v_validity').": ".date('d/m/Y', strtotime($voucher->expired_date))}}</p>
            </div>
            <div class="save-voucher">
                <a href="#" class="save_btn">Add to your voucher</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="info-brand" id="info-brand">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 mx-auto wrap">
            @if(!empty($listCity))
              <select class="selectpicker city-select" data-live-search="true" data-live-search-placeholder="{{trans('content.search')}}"  title="{{trans('content.filter_by_location')}}" data-width="100%"  data-size="8">
                @foreach($listCity as $item)
                  @if(count($item) > 0)
                    <option value="{{$item[0]->city_id}}">
                      {{Translate::transObj($item[0],'city_name')}}
                    </option>
                  @endif
                @endforeach
              </select>
            @endif
            <div class="listCateBrand">
              <div class="list-brand popular">
                <h2 class="heading">{{trans('content.popular_brand')}}</h2>
                <div class="list-item">
                  <a href="javascript:void(0)" class="brand_wr" data-brandid="101">
                    <img class="logo-brand" src="https://img.gotit.vn/compress/brand/2017/06/1498656338_Xt8Y3.png">
                  </a>
                  <a href="javascript:void(0)" class="brand_wr" data-brandid="63">
                    <img class="logo-brand" src="https://img.gotit.vn/compress/brand/1507286551_cmUef.png" alt="Highlands Coffee">
                  </a>
                  <a href="javascript:void(0)" class="brand_wr" data-brandid="56">
                    <img class="logo-brand" src="https://img.gotit.vn/compress/brand/1476931527_pEEGt.png" alt="GoGi House">
                  </a>
                  <a href="javascript:void(0)" class="brand_wr" data-brandid="57">
                    <img class="logo-brand" src="https://img.gotit.vn/compress/brand/1476931477_UzeYo.png" alt="Kichi Kichi ">
                  </a>
                  <a href="javascript:void(0)" class="brand_wr" data-brandid="68">
                    <img class="logo-brand" src="https://img.gotit.vn/compress/brand/1476349399_Hykct.png" alt="SumoBBQ">
                  </a>
                  <a href="javascript:void(0)" class="brand_wr" data-brandid="26">
                    <img class="logo-brand" src="https://img.gotit.vn/compress/brand/1448707019_AZeJo.png" alt="Phuc Long ">
                  </a>
                  <a href="javascript:void(0)" class="brand_wr" data-brandid="110">
                    <img class="logo-brand" src="https://img.gotit.vn/compress/brand/2018/01/1515644472_tlqht.png" alt="BoBaPoP">
                  </a>
                  <a href="javascript:void(0)" class="brand_wr" data-brandid="6">
                    <img class="logo-brand" src="https://img.gotit.vn/compress/brand/1519698965_soR4Y.png" alt="TOUS les JOURS">
                  </a>
                  <a href="javascript:void(0)" class="brand_wr" data-brandid="91">
                    <img class="logo-brand" src="https://img.gotit.vn/compress/brand/2017/02/1487923827_yNVCf.png" alt="Ministop">
                  </a>
                  <a href="javascript:void(0)" class="brand_wr" data-brandid="7">
                    <img class="logo-brand" src="https://img.gotit.vn/compress/brand/2015/10/1445251857_UZ373.png" alt="SHOP &amp; GO">
                  </a>
                </div>
              </div>
              @if(!empty($listCategory))
                @foreach($listCategory as $category)
                  @if(count($category) > 0)
                    <div class="list-brand">
                      <h2 class="heading">{{Translate::transObj($category[0],'cate_name')}}</h2>
                      <div class="list-item">
                        @foreach($category as $brandItem)
                        <a href="javascript:void(0)" class="brand_wr" data-brandid="{{$brandItem->brand_id}}">
                          <img class="logo-brand" src="{{env('IMG_SERVER').$brandItem->brand_img_path}}" alt="{{Translate::transObj($brandItem,'brand_name')}}">
                        </a>
                        @endforeach
                      </div>
                    </div>
                  @endif
                @endforeach
              @endif
            </div>
            <div class="list-brand topup">
              <h2 class="heading">{{trans('content.topup')}}</h2>
              <div class="list-item">
                <a href="javascript:void(0)" class="brand_wr">
                  <img class="logo-brand" src="https://img.gotit.vn/compress/brand/2016/08/1471921408_KxS6U.png" alt="Viettel">
                </a>
                <a href="javascript:void(0)" class="brand_wr">
                  <img class="logo-brand" src="https://img.gotit.vn/compress/brand/2016/08/1471920885_6tISg.png" alt="Mobilephone">
                </a>
                <a href="javascript:void(0)" class="brand_wr">
                  <img class="logo-brand" src="https://img.gotit.vn/compress/brand/2016/08/1471921291_yINyr.png" alt="Vinaphone">
                </a>
                <a href="javascript:void(0)" class="brand_wr">
                  <img class="logo-brand" src="../layouts/v4/img/vietnamoblie.png" alt="Vietnammobi">
                </a>
                <a href="javascript:void(0)" class="brand_wr">
                  <img class="logo-brand" src="../layouts/v4/img/Gmobile.png" alt="Gmobi">
                </a>
              </div>
            </div>
            <!-- <div  class="showmore" style="">
               <a href="#" id="loadMore">{{trans('content.viewmore_category',['number'=>3])}}</a>
               <a href="#" id="hideMore" style="display: none">{{trans('content.viewless')}}</a>
            </div> -->
            <div class="note-brand">
              <p>{{trans('content.note_brand_accept')}}</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- <section class="info-topup" id="info-topup">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 mx-auto wrap">
            <div class="list-brand">
              <h2 class="heading">{{trans('content.topup')}}</h2>
              <div class="list-item">
                <a href="javascript:void(0)" class="brand_wr">
                  <img class="logo-brand" src="https://img.gotit.vn/compress/brand/2016/08/1471921408_KxS6U.png" alt="Viettel">
                </a>
                <a href="javascript:void(0)" class="brand_wr">
                  <img class="logo-brand" src="https://img.gotit.vn/compress/brand/2016/08/1471920885_6tISg.png" alt="Mobilephone">
                </a>
                <a href="javascript:void(0)" class="brand_wr">
                  <img class="logo-brand" src="https://img.gotit.vn/compress/brand/2016/08/1471921291_yINyr.png" alt="Vinaphone">
                </a>
                <a href="javascript:void(0)" class="brand_wr">
                  <img class="logo-brand" src="../layouts/v4/img/vietnamoblie.png" alt="Vietnammobi">
                </a>
                <a href="javascript:void(0)" class="brand_wr">
                  <img class="logo-brand" src="../layouts/v4/img/Gmobile.png" alt="Gmobi">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> -->
    <section class="footer-info">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 wrap">
            <p class="how-use"><a href="https://www.gotit.vn/egift#faqs">{{trans('content.how_use_this_vc')}}</a></p>
            <p class="hotline">Got It Hotline: 1900 5588 20</p>
            <p class="view-nearest"><a href="javascript:void(0)">{{trans('content.view_nearest_store')}}</a></p>
          </div>
        </div>
      </div>
    </section>
    <!-- Topup modal -->
    <div id="topupModal" class="modal topupModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <!-- <div class="modal-header">
            <h5 class="modal-title">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div> -->
          <div class="modal-body">
            <img src="https://img.gotit.vn/group_phone2.png">
            <div class="error_phone" style="color:#ff5f5f;text-align: left;display:none"></div>
            <p>{{trans('content.note_topup')}}</p>
            <input type="text" name="phone_number" value="" class="form-control phone_number" placeholder="{{trans('content.phone_number')}}">
            <input type="hidden" value="{{$voucher->code}}" name="code">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn topupBtn">{{trans('content.topup_now')}}</button>
            <button type="button" class="btn btn-close" data-dismiss="modal"><span class="icon-close"></span>{{trans('content.close')}}</button>
          </div>
        </div>
      </div>
    </div>

    <style type="text/css">
      
    </style>
    <section id="brand-detail" class="brand-detail" style="">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 wrap">
            <h2 class="heading"><span class="close-brand-detail"></span>Brand Details</h2>
            <div class="brand-detail-info">
              <img src="">
              <h3 class="brand-name"></h3>
              <p class="short-desc"></p>
           
            </div>
            <div class="store-info">
              <div class="top-info">
                <h2 class="float-left"><?php echo trans('content.stores')?></h2>
                <span class="float-right"></span>
              </div>
              <select class="selectpicker city-brandDetail" data-live-search="true" data-live-search-placeholder="{{trans('content.search')}}"  title="{{trans('content.filter_by_location')}}" data-width="100%"  data-size="8">
                
              </select>
              <ul class="list-store">
                 
              </ul>
              <div  class="showmore" style="">
                 <a href="#" id="loadMore" style="display: none;">View more stores</a>
                 <a href="#" id="hideMore" style="display: none">View less</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section id="nearest-detail" class="nearest-detail" style="">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 wrap">
            <h2 class="heading"><span class="close-nearest-detail"></span>{{trans('content.nearest_store')}}</h2>
            <p class="sub-title">{{trans('content.filtered_within_km')}}</p>
            
            <div class="store-info">
              <div class="top-info">
                <h2 class="float-left">{{trans('content.stores')}}</h2>
                <span class="float-right"></span>
              </div>

              @if(!empty($listCategory))
                <select class="selectpicker category-store-select" data-live-search="true" data-live-search-placeholder="{{trans('content.search')}}"  title="{{trans('content.all_category')}}" data-width="100%"  data-size="8">
                  @foreach($listCategory as $category)
                    @if(count($category) > 0)
                      <option value="{{$category[0]->category_id}}">
                        {{Translate::transObj($category[0],'cate_name')}}
                      </option>
                    @endif
                  @endforeach
                </select>
              @endif
              <ul class="list-store">
                 
              </ul>
              <!-- <div  class="showmore" style="">
                 <a href="#" id="loadMore">View more 3 stores</a>
                 <a href="#" id="hideMore" style="display: none">View less</a>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="loading">
      <div id='img-loading' class='uil-spin-css' style="-webkit-transform:scale(0.4)"><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div></div>
    </div>

    <input name="store_group_id" type="hidden" value="{{$product->store_group_id}}"/>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="img_server" value="{{env('IMG_SERVER')}}">
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{!! asset('layouts/v4/js/jquery.min.js')  !!}"></script>
    <script type="text/javascript" src="{!! asset('layouts/v4/js/bootstrap.bundle.min.js')  !!}"></script>
    <script type="text/javascript" src="{!! asset('layouts/v4/js/bootstrap-select.min.js')  !!}"></script>
    <script type="text/javascript" src="{!! asset('layouts/v4/js/jquery.easing.min.js')  !!}"></script>
    <script type="text/javascript" src="{!! asset('layouts/v4/js/jquery.show-more.js')  !!}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
  
    <!-- Custom scripts for this template -->
    <!-- <script src="js/new-age.min.js"></script> -->
    <script type="text/javascript">
      var img_serve = $('input[name="img_server"]').val();
      var locations = [];
      var typeFind = '';
      var brandIdChoose = '';
      var store_group_id = $('input[name="store_group_id"]').val();
       //loadmore brand detail
      var size_listItem_brand = 0;
      var x=20;
      <?php
        $ListBrandId = array();
        foreach ($list_brand_id as $key => $value) {
          $ListBrandId[$key] =  $value;
        }
       echo 'var list_brand_id = '.json_encode($ListBrandId).';';
      ?>
      
      $(document).ready(function(){
        //getlistCategoryBrandByCity
        // $(".info-brand .list-brand").slice(0, 3).show();
        // if ($(".info-brand .list-brand:hidden").length != 0) {
        //   $(".info-brand #loadMore").show();
        // }   
        // $(".info-brand #loadMore").on('click', function (e) {
        //   e.preventDefault();
        //   $(".info-brand .list-brand:hidden").slice(0, 3).slideDown();
        //   if ($(".info-brand .list-brand:hidden").length == 0) {
        //     $(".info-brand #loadMore").hide();
        //     $(".info-brand #hideMore").show();
        //   }
        // });
        // $(".info-brand #hideMore").on('click', function (e) {
        //   e.preventDefault();
        //   $(".info-brand .list-brand").slice(3, 6).slideUp();
        //     $(".info-brand #loadMore").show();
        //     $(".info-brand #hideMore").hide();
        // });

        $('.brand-detail #loadMore').click(function () {
            x= (x+10 <= size_listItem_brand) ? x+10 : size_listItem_brand;
            $('.brand-detail .list-store li:lt('+x+')').show();

            if (x==size_listItem_brand) {
              $('.brand-detail .store-info .showmore').hide();
              $(".brand-detail #loadMore").hide();
            } 
        });
        


        $('.menu_icon').click(function(e){
          e.preventDefault();
          $(this).toggleClass('showMenu');
          $('.list_menu').toggleClass('showMenu');
          $('body').toggleClass('showMenu');
        });
        
        $(".list-brand.topup .list-item .brand_wr").click(function(){
          $('.error_phone').text('');
          $('#topupModal').modal({
            'show':true,
            'backdrop':'static'
          })
        });

        var lang = $('html').attr('lang');
        $('.short-desc').showMore({
            minheight: 35,
            buttontxtmore: (lang == 'vi' ? 'Xem thêm':'View more'),
            buttontxtless: (lang == 'vi' ? 'Ẩn bớt':'View less'),
            animationspeed: 250,
            buttoncss: 'viewmoreBrand'
        });

        $('body').on('click','.info-brand .list-brand:not(".topup") .brand_wr',function(e){
          e.preventDefault();
          $('.loading').css({'display':'block'});
          $('.brand-detail #loadMore').hide();
          $('.brand-detail .store-info .showmore').hide();
          typeFind="filterBrand";
          var lang = $('html').attr('lang');
          var brand_id = $(this).data().brandid;
          brandIdChoose = parseInt(brand_id);
          var city_id = $('.selectpicker.city-select').val();
          $(".brand-detail ul.list-store").empty();
          $('.brand-name').text('');
          $('.short-desc').text('');
          
          //get list city by brand
          setTimeout(function(){
            $.when(
              $.ajax({
                url: "/voucher/getstore",
                type: 'POST',
                data: {brand_id: brand_id,store_group_id:store_group_id,city_id:city_id},
                dataType: 'json',
                async:false,
                success: function(data){
                  locations = [];

                  $('.brand-detail-info img').prop('src',img_serve+''+data[0].img_path);
                  $('.brand-detail-info .brand-name').text(lang == 'vi' ? data[0].brand_name_vi : data[0].brand_name_vi);
                  $('.short-desc').text(lang == 'vi' ? data[0].desc_vi : data[0].desc_en);
                  $.each(data, function(index, storeObj){
                    var store = {
                      'storeNm' : (lang == 'vi' ? storeObj.name_vi : storeObj.name_en),
                      'brandNm' : (lang == 'vi' ? storeObj.brand_name_vi : storeObj.brand_name_en),
                      'lat' : storeObj.lat,
                      'long' : storeObj.long,
                      'storeAddr' : (lang == 'vi' ? storeObj.address_vi : storeObj.address_en),
                      'phone' : storeObj.phone,
                    }
                    locations.push(store);
                  });
                }
              }),
              $.ajax({
                url:'/voucher/getCityByBrand',
                type:'POST',
                data:{brand_id:brand_id},
                dataType:'json',
                async:false,
                success:function(response){
                   //add city in dropdown filter
                    $('select.city-brandDetail').empty();
                    $.each(response['list_city'],function(index,city){
                        if(lang == 'vi'){
                            $("select.city-brandDetail").append('<option value="'+city.city_id+'" id="'+city.city_id+'">'+city.city_name_vi+'</option>');
                        }
                        else{
                            $("select.city-brandDetail").append('<option value="'+city.city_id+'" id="'+city.city_id+'">'+city.city_name_en+'</option>');
                        }

                    }); 
                    $('select.city-brandDetail').selectpicker('refresh');
                    if(city_id){
                      $('select.city-brandDetail').selectpicker('val', city_id);
                    }
                    
                }
              }),
              //get list store theo brand va storegroupid
              
            ).then(function() {
              $('.brand-detail .top-info span').text(locations.length +' <?php echo trans("content.stores")?>');
              FindNear();
              setTimeout(function(){
                size_listItem_brand =  $(".brand-detail .list-store li").length; 
                if (size_listItem_brand >= x) {
                  $(".brand-detail #loadMore").show();
                  $('.brand-detail .store-info .showmore').show();
                }
                
                $('.brand-detail .list-store li:gt('+x+')').hide();

                
              },500);
              $('.loading').css({'display':'none'});
              $('.brand-detail').css({'right':0, '-webkit-transition':'0.3','-moz-transition':'0.3','transition':'0.3'});
              $('body').css({'position':'fixed'});
              $('.short-desc').showMore({
                minheight: 35,
                buttontxtmore: (lang == 'vi' ? 'Xem thêm':'View more'),
                buttontxtless: (lang == 'vi' ? 'Ẩn bớt' : 'View less'),
                animationspeed: 250,
                buttoncss: 'viewmoreBrand'
              });
            })
          },1000);
          
          
        });


        //select box city in brand detail change
        $('.selectpicker.city-brandDetail').on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
          $('.brand-detail #loadMore').hide();
          var city_id = $(e.currentTarget).val();
          var brand_id = brandIdChoose;
          var lang = $('html').attr('lang');
          $('.loading').css({'display':'block'});
          setTimeout(function(){
            $.ajax({
              url: "/voucher/getstore",
              type: 'POST',
              data: {brand_id: brand_id,store_group_id:store_group_id,city_id:city_id},
              dataType: 'json',
              async:false,
              success: function(data){
                locations = [];
                // console.log(data);
                $('.brand-detail-info img').prop('src',img_serve+''+data[0].img_path);
                $('.short-desc').text(lang == 'vi' ? data[0].desc_vi : data[0].desc_en);
                $.each(data, function(index, storeObj){
                  var store = {
                    'storeNm' : (lang == 'vi' ? storeObj.name_vi : storeObj.name_en),
                    'brandNm' : (lang == 'vi' ? storeObj.brand_name_vi : storeObj.brand_name_en),
                    'lat' : storeObj.lat,
                    'long' : storeObj.long,
                    'storeAddr' : (lang == 'vi' ? storeObj.address_vi : storeObj.address_en),
                    'phone' : storeObj.phone,
                  }
                  locations.push(store);
                });
              }
            }).done(function(){
              $('.loading').css({'display':'none'});
              $('.brand-detail .top-info span').text(locations.length +' <?php echo trans("content.stores")?>');
              FindNear();
              setTimeout(function(){ 
                size_listItem_brand =  $(".brand-detail .list-store li").length;
                if (size_listItem_brand >= x) {
                  $(".brand-detail #loadMore").show();
                }
                $('.brand-detail .list-store li:gt('+x+')').hide();
              },200);
            })
          },1000);
        });

        $('.close-brand-detail').click(function(){
          typeFind = '';
          $('.brand-detail').css({'right':'-100%', '-webkit-transition':'0.3s','-moz-transition':'0.3s','transition':'0.3s'});
          $('.brand-detail .store-info .showmore').show();
          $(".brand-detail #loadMore").show();
          $('body').css({'position':'relative'});
        });

        $('.view-nearest a').click(function(){
          typeFind="filterNearest";
          <?php
            $storeArr = array();
            foreach ($stores as $store) {
            $storeArr[] = array(
            'storeNm'  => Translate::transObj($store, 'name'),
            'brandNm'  => Translate::transObj($store, 'brand_name'),
            'lat'   => $store->lat,
            'long'   => $store->long,
            'storeAddr'   =>  Translate::transObj($store, 'address'),
            'phone' => $store->phone
            );
            }
            echo 'var listStore = '.json_encode($storeArr).';';
          ?>

          $('.nearest-detail .top-info span').text(listStore.length +' <?php echo trans('content.stores')?>');
          locations = listStore;
          $('.loading').css({'display':'block'});
          setTimeout(function(){
            FindNear();
            $('.loading').css({'display':'none'});
            $('.nearest-detail').css({'right':0,'-webkit-transition':'0.3s','-moz-transition':'0.3s','transition':'0.3s'});
            $('body').css({'position':'fixed'});
          },300); 

        });

        // getcatebrandbycity
        $('.selectpicker.city-select').on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
          var city_id = $(e.currentTarget).val();
          var listBrandId = list_brand_id;
          var lang = $('html').attr('lang');
          var countCate = 0;
          setTimeout(function(){
            $.ajax({
              url:'/voucher/getcatebrandbycity',
              method:'POST',
              data:{city_id:city_id,list_brand_id:list_brand_id},
              async:false,
              success:function(response){

                if(response){
                  $('.info-brand .list-brand').not('.popular,.topup').remove();
                  countCate = Object.keys(response.listCateBrand).length;
                  $('.list-brand.popular .list-item a').css({'display':'none'});
                  
                  $.each(response.listCateBrand, function(index, value){ 
                    
                    var html = '';
                    
                    html += '<div class="list-brand" style="display:none">';
                    html += '<h2>'+(lang == 'vi' ? value[0].cate_name_vi : value[0].cate_name_en ) +'</h2>';
                    html += '<div class="list-item">';
                    $.each(value, function(i, item){
                      $('.list-brand.popular .list-item').find('a[data-brandid="'+item.brand_id+'"]').css({'display':'inline-block'});
                      html += '<a href="javascript:void(0)" class="brand_wr" data-brandid='+item.brand_id+'>'+
                        '<img class="logo-brand" src="https://img.gotit.vn/'+item.brand_img_path+'"/>'+
                      '</a>';
                    });
                    html += '</div>';
                    html += '</div>';
                    $('.info-brand .listCateBrand').append(html);
                  })  
                  console.log($(".list-brand.popular .list-item a:visible").length);
            
                  setTimeout(function(){
                    if ($(".info-brand .list-brand.popular .list-item a:visible").length == 0) {
                        $(".info-brand .list-brand.popular").hide();
                    }
                    else {
                        $(".info-brand .list-brand.popular").show();
                    }
                  },100);
                }
              },
            }).done(function(){
             
              $(".info-brand #hideMore").hide();
              $(".info-brand .list-brand").slice(0, 3).show();
              if ($(".info-brand .list-brand:hidden").length != 0) {
                $(".info-brand #loadMore").text((lang == "vi" ? "Xem thêm "+(countCate - 2)+" thể loại" : "View more "+(countCate - 2)+" categories"));
                $(".info-brand #loadMore").show();
              }   
              else{
                // $(".info-brand .showmore").hide();
                $(".info-brand #loadMore").hide();
              }
            });
          },1000);
        });

        //select box category in store detail change
        $('.selectpicker.category-store-select').on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
          var category_id = $(e.currentTarget).val();
          console.log(category_id);
          var brand_id = brandIdChoose;
          var lang = $('html').attr('lang');
          var city_id = 0;
          $('.loading').css({'display':'block'});
          setTimeout(function(){
            $.when(
              $.ajax({
                url:'/voucher/getbrand',
                method:'POST',
                data:{cate_id:category_id,store_group_id:store_group_id},
                async:false,
                success:function(response){
                  //call list brand -> trong do call từng brand ajax
                 
                  if(response['list_brand'].length > 0){
                   
                    locations = [];
                    $.each(response['list_brand'],(index,brand)=>{
                      var brand_id_item = brand.brand_id;
                      $.ajax({
                        url: "/voucher/getstore",
                        type: 'POST',
                        data: {brand_id: brand_id_item,store_group_id:store_group_id,city_id:city_id},
                        dataType: 'json',
                        async:false,
                        success: function(data){
                         
                          $('.brand-detail-info img').prop('src',img_serve+''+data[0].img_path);
                          $('.short-desc').text(lang == 'vi' ? data[0].desc_vi : data[0].desc_en);
                          $.each(data, (index, storeObj) =>{
                            var store = {
                              'storeNm' : (lang == 'vi' ? storeObj.name_vi : storeObj.name_en),
                              'brandNm' : (lang == 'vi' ? storeObj.brand_name_vi : storeObj.brand_name_en),
                              'lat' : storeObj.lat,
                              'long' : storeObj.long,
                              'storeAddr' : (lang == 'vi' ? storeObj.address_vi : storeObj.address_en),
                              'phone' : storeObj.phone,
                            }
                            locations.push(store);
                          });
                        }
                      })
                    });
                  }
                },
              })
            ).then(function(){
              $('.loading').css({'display':'none'});
              $('.nearest-detail .top-info span').text(locations.length +' <?php echo trans('content.stores')?>');
              FindNear();
            })
          },1000);
          // $.ajax({
          //   url:'/voucher/getbrand',
          //   method:'POST',
          //   data:{cate_id:category_id,store_group_id:store_group_id},
          //   async:false,
          //   success:function(response){
          //     //call list brand -> trong do call từng brand ajax
             
          //     if(response['list_brand'].length > 0){
               
          //       locations = [];
          //       $.each(response['list_brand'],(index,brand)=>{
          //         var brand_id_item = brand.brand_id;
          //         $.ajax({
          //           url: "/voucher/getstore",
          //           type: 'POST',
          //           data: {brand_id: brand_id_item,store_group_id:store_group_id,city_id:city_id},
          //           dataType: 'json',
          //           async:false,
          //           success: function(data){
                     
          //             $('.brand-detail-info img').prop('src',img_serve+''+data[0].img_path);
          //             $('.short-desc').text(lang == 'vi' ? data[0].desc_vi : data[0].desc_en);
          //             $.each(data, (index, storeObj) =>{
          //               var store = {
          //                 'storeNm' : (lang == 'vi' ? storeObj.name_vi : storeObj.name_en),
          //                 'brandNm' : (lang == 'vi' ? storeObj.brand_name_vi : storeObj.brand_name_en),
          //                 'lat' : storeObj.lat,
          //                 'long' : storeObj.long,
          //                 'storeAddr' : (lang == 'vi' ? storeObj.address_vi : storeObj.address_en),
          //                 'phone' : storeObj.phone,
          //               }
          //               locations.push(store);
          //             });
          //           }
          //         })
          //       });
          //     }
          //   },
          // }).done(function(){
          //   $('.loading').css({'display':'none'});
          //   $('.nearest-detail .top-info span').text(locations.length +' <?php echo trans('content.stores')?>');
          //   FindNear();
          // })
        });

        $('.close-nearest-detail').click(function(){
          typeFind = '';
          $('.nearest-detail').css({'right':'-100%', '-webkit-transition':'0.3s','-moz-transition':'0.3s','transition':'0.3s'});
          $('body').css({'position':'relative'});
        });

        //topup
        $('.topupBtn').click(function(){
          var lang = $('html').attr('lang');
          var phone_val = $('input[name="phone_number"]').val();
          //var amount = $('input[name="price_value"]').val();
          //var amount = '10000';
          var code = $('input[name="code"]').val();

          var err = '';
          if(phone_val.length == 0){
            if(lang == 'vi'){
                err += '<p>Vui lòng nhập số điện thoại</p>';
            }
            else{
                err += '<p>Please enter the phone number</p>';
            } 
          }
          else if(phone_val.length > 11 || phone_val.length < 10){
            if(lang == 'vi'){
                err += '<p>Số điện thoại không được ít hơn 10 số hoặc nhiều hơn 11 số</p>';
            }
            else{
                err += '<p>Phone numbers must not be less than 10 digits or more than 11 digits</p>';
            }
              
          }
          else{
            var regExp = /^(09|08|01[2689])\d{8}$/; 
            var checkphone = phone_val.match(regExp);
            if (!checkphone) {
                if(lang == 'vi'){
                    err += '<p>Số điện thoại không hợp lệ, vui lòng kiểm tra lại</p>';
                }
                else{
                    err += '<p>Phone number is invalid, please check again</p>';
                }
            }
          }

          if(err != '' || err.length > 0){
            $('.error_phone').html('').append(err).css({'display':'block'});
            return false;
          }
          else{
            $('.error_phone').html('');
            $('.loading').css({'display':'block'});
            // $('#img-loading').show();
            var GotitRequest = {
                phoneNumber:phone_val,code:code
            };

            var hasResult = false;

            $.ajax({
              url:'<?php echo env('PHONE_TOPUP_URL','https://topup.gotit.vn:9898');?>/topup',//call lumen project
              async: true,
              crossDomain: true,
              type:'POST',
              dataType:'json',
              contentType: "application/json; charset=utf-8",
              headers: {
                  "Content-Type": "application/json"
              },
              data: JSON.stringify(GotitRequest),
              timeout:30000,
              success:function(data){
                var msg = '';
                if(lang == 'vi'){
                    if(data.stt == -2 || data.stt == -1 || data.stt == -3){
                        msg = 'Voucher không hợp lệ';
                    }
                    else if(data.stt == 0){
                        msg = 'Voucher đã sử dụng';
                    }
                    else if(data.stt == 1){
                        msg = 'Nạp tiền thành công';
                    }
                    else if(data.stt == 3){
                        msg = 'Nạp tiền thất bại';
                    }
                    else if(data.stt == 2){
                        msg = 'Đang xử lý quá trình nạp tiền';
                    }
                    else if(data.stt == 4){
                        msg = 'Quá nhiều yêu cầu từ máy bạn. Vui lòng tải lại trang và thử lại sau khoảng 1 phút.';
                    }
                    else if(data.stt == 5){
                        msg = 'Giao dịch top-up thất bại do lỗi từ telco: SỐ ĐT KHÔNG HỢP LỆ.';
                    }
                    else if(data.stt == 6){
                        msg = 'Thuê bao không có tài khoản EZPay (trường hợp của Vinaphone).';
                    }
                    else if(data.stt == 7){
                        msg = 'Thuê bao đang bị khóa chiều nạp.';
                    }
                }
                else{
                    if(data.stt == -2 || data.stt == -1){
                        msg = 'Voucher Invalid';
                    }
                    else if(data.stt == 0){
                        msg = 'Voucher has used';
                    }
                    else if(data.stt == 1){
                        msg = 'Topup card success';
                    }
                    else if(data.stt == 3){
                        msg = 'Topup card fail';
                    }
                    else if(data.stt == 2){
                        msg = 'Topup card processing';
                    }
                    else if(data.stt == 4){
                        msg = 'Too many request, please refresh page and try again in a minuste.';
                    }
                    else if(data.stt == 5){
                        msg = 'Topup failed: Phone number is not valid';
                    }
                    else if(data.stt == 6){
                        msg = 'This phone number has no EZPay account (Vinaphone).';
                    }
                    else if(data.stt == 7){
                        msg = 'Phone number is blocked to topup.';
                    }
                }
                
                $('.phontopup .form-topup').html('').append('<p style="text-align:center; position:absolute;top:50%;left:50%;transform: translate(-50%,-50%);font-size:16px;font-weight:500;margin-top:-15px;">'+msg+'</p>');

                if(data.stt != 2){
                    hasResult = true;
                }
              },

              error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(ajaxOptions);
                console.log(thrownError);

                if(ajaxOptions==="timeout") {
                    $('.loading').css({'display':'none'});

                    if(lang == 'vi')
                        msg = 'Đang xử lý quá trình nạp tiền';
                    else
                        msg = 'Topup card processing';
                    $('.phontopup').html('').append('<p><b>'+msg+'</b></p>');

                    if(!hasResult)
                        checktransacion(code, lang);
                }
              }
            }).done(function(){
              // $('#img-loading').hide();
              $('.loading').css({'display':'none'});
              if(!hasResult)
                  checktransacion(code, lang);
            });
          }
      });


      $('a.btn-language').click(function() {
        var curr_lang = $('html').attr('lang');
        var lang = $(this).data().lang;
        if(lang == 'vi'){
          var next_lang = 'en';
        }
        else{
          var next_lang = 'vi';
        }
        $.ajax({
            url: '/change-language',
            type: 'post',
            data: {lang: next_lang},
            success: function(){
                location.reload();
            }
        });
      });




      });


    $(".phone_number").keypress(validateNumber);
    function validateNumber(event) {
        event = window.event;
        var key = window.event ? event.keyCode : event.which;

        if (event.keyCode === 8 || event.keyCode === 46
         || event.keyCode === 37 || event.keyCode === 39) {
            return true;
        }
        else if ( key < 48 || key > 57 ) {
            return false;
        }
        else return true;
    };

    function checkPermisionGeoLocation(){
      navigator.permissions.query({name:'geolocation'}).then(function(result) {
        if (result.state === 'denied') { 
          return false;
        } else{
            return true;
        }
      });
    }

    function FindNear()
    {      
      if (checkPermisionGeoLocation) {
        console.log('enable location')
        // HTML5/W3C Geolocation
        if ( navigator.geolocation )
        {
            navigator.geolocation.getCurrentPosition( UserLocation, errorCallback,{maximumAge:60000,enableHighAccuracy: true,timeout:10000});
        }
        else{
          ClosestLocation( 10.767661, 106.6999359, "254 Nguyen Cong Tru" );
        }   
      }else{
        alert('<?php echo trans("content.enable_location")?>');
      }
      
    }
    function errorCallback( error )
    { 
      console.log(error);
      if(error.code == 1){
        alert('<?php echo trans('content.enable_location')?>')
        return false;
      }
      
    }
    // Callback function for asynchronous call to HTML5 geolocation
    function UserLocation( position )
    {
        ClosestLocation( position.coords.latitude, position.coords.longitude, "This is my Location" );
    }

    // Convert Degress to Radians
    function Deg2Rad( deg ) {
       return deg * Math.PI / 180;
    }

    // Get Distance between two lat/lng points using the Haversine function
    // First published by Roger Sinnott in Sky & Telescope magazine in 1984 (“Virtues of the Haversine”)
    
    function Haversine( lat1, long1, lat2, long2 )
    {
        var R = 6372.8; // Earth Radius in Kilometers

        var dLat = Deg2Rad(lat2-lat1);  
        var dlong = Deg2Rad(long2-long1);  

        var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(Deg2Rad(lat1)) * Math.cos(Deg2Rad(lat2)) * Math.sin(dlong/2) * Math.sin(dlong/2);  
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        var d = R * c; 

        // Return Distance in Kilometers
        return d;
    }

    // Display a map centered at the nearest location with a marker and InfoWindow.
    function ClosestLocation( lat, long, title )
    {
        // find the closest location to the user's location
        var closest = 0;
        var mindist = 99999;

        for(var i = 0; i < locations.length; i++) 
        {
            // get the distance between user's location and this point
            var dist = Haversine( locations[ i ].lat, locations[ i ].long, lat, long );
            locations[i]['distance'] = dist;
        }
        if(typeFind == 'filterNearest'){
          locations = locations.filter(function(x) {
              return x.distance < 1;
          });
          locations.sort(function(a,b) { 
            return parseFloat(a.distance) - parseFloat(b.distance) ;
          });
        }
        else{
          locations.sort(function(a,b) { 
            return parseFloat(a.distance) - parseFloat(b.distance) ;
          });
        }
      
        // Create a Google coordinate object for the closest location
        var browser = navigator.userAgent;
        var lang = $('html').attr('lang'); 
        var ulClass="";
        $('.no-store').html('');
        console.log(typeFind)
        if(typeFind == 'filterBrand'){
          $(".brand-detail ul.list-store").empty();
          ulClass = '.brand-detail ul.list-store';
        }
        else if(typeFind == 'filterNearest'){
          $(".nearest-detail ul.list-store").empty();
          ulClass = '.nearest-detail ul.list-store';
        }
        $('.top-info span').text(locations.length + ' <?php echo trans('content.stores')?>');
        if(locations.length > 0){
          $.each(locations,function(index, storeObj){
            var store_name = storeObj.storeNm;
            var store_address = storeObj.storeAddr;
            var brand_name = storeObj.brandNm;
            var phoneText = "Số điện thoại: ";
            var addressText= "Địa chỉ: ";
            var distance = parseFloat(storeObj.distance).toFixed(2)+" KM";
            if(lang != 'vi'){
                phoneText = "Phone: ";
                addressText= "Address: ";
            }

            if(browser.match(/(iPhone|iPod|iPad)/)){
                var link = 'comgooglemaps://?daddr='+store_address+'&directionsmode=driving';
            } 
            else if(browser.match(/Android/)){
                var link ='google.navigation:q='+store_address+'&'+storeObj.lat+','+storeObj.long;
            }

            $(ulClass).append('<li><p class="goto-location">' + 
                brand_name + ' - ' + store_name +'<a href="'+link+'"></a></p>'+
                '<p class="address"><span>' +addressText+ store_address + '</span><br><span>'+
                (storeObj.phone != null && storeObj.phone != '' ? phoneText+storeObj.phone : '')+
                '</span><span class="distance">'+distance+'</span></p>'+
                '</li>'
            );
          }); 

        }
        else{
          $('.store-info').append('<div class="no-store"><p style="text-align:center">No store nearest here</p></div>');
        }
        
        
    }


    var loop = 30;
    var ind = 0;
    //loop for 30 times
    function checktransacion(code, lang){
      var hasResult = false;
      var request = {
          code:code
      };

      setTimeout(function() {
          if (ind <= loop) {
              ind++;

              $.ajax({
                  url: '<?php echo env('PHONE_TOPUP_URL','https://topup.gotit.vn:9898');?>/checkprocessing',//call lumen project
                  async: true,
                  crossDomain: true,
                  type: 'POST',
                  dataType: 'json',
                  contentType: "application/json; charset=utf-8",
                  headers: {
                      "Content-Type": "application/json"
                  },
                  data: JSON.stringify(request),

                  success: function (data) {
                      if(lang == 'vi'){
                          if (data.stt == 0) {
                              msg = 'Nạp tiền thành công';
                          }
                          else if (data.stt != 99 && data.stt != 100) {
                              msg = 'Nạp tiền thất bại';
                          }
                      }
                      else {
                          if (data.stt == 0) {
                              msg = 'Topup phone successfully';
                          }
                          else if (data.stt != 99 && data.stt != 100) {
                              msg = 'Topup phone failed';
                          }
                      }

                      if (data.stt == 0 || (data.stt != 99 && data.stt != 100)) {
                          $('.phontopup').html('').append('<p><b>' + msg + '</b></p>');
                          ind = loop + 1;//break the loop
                      }
                  },

                  error: function (xhr, ajaxOptions, thrownError) {
                      console.log(xhr.status);
                      console.log(ajaxOptions);
                      console.log(thrownError);
                  }
              });

              checktransacion(code);
          }
      }, 5000);
    }
    </script>
  </body>
  </html>