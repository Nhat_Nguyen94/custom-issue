<?php
namespace Dayone\Issuer;

use Illuminate\Support\ServiceProvider;

class GrabServiceProvider extends ServiceProvider{

    public function boot()
    {   
        // $this->loadViewsFrom(__DIR__.'/Views', 'issue');
       
    }

    public function register()
    {
         $this->loadViewsFrom(__DIR__.'/Views/Grab', 'Grab');
    }
    
}