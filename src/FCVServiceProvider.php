<?php
namespace Dayone\Issuer;

use Illuminate\Support\ServiceProvider;

class FCVServiceProvider extends ServiceProvider{

    public function boot()
    {   
        // $this->loadViewsFrom(__DIR__.'/Views', 'issue');
       
    }

    public function register()
    {
         $this->loadViewsFrom(__DIR__.'/Views/FCV', 'FCV');
    }
    
}