<?php
namespace Dayone\Issuer;

class Halliburton {

    public function __construct(){

    }

    public function view()
    {
        \App::register('Dayone\Issuer\HalliburtonServiceProvider');
        return 'Halliburton::index';
    }

}