<?php
namespace Dayone\Issuer;

class HSC_June
{

    public function __construct()
    {

    }

    public function view()
    {
        \App::register('Dayone\Issuer\HSCServiceProvider');
        return 'HSC::hsc_june';
    }
}
