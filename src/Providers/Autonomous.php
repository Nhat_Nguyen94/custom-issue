<?php
namespace Dayone\Issuer;

class Autonomous {

    public function __construct(){

    }

    public function view()
    {
        \App::register('Dayone\Issuer\AutonomousServiceProvider');
        return 'Autonomous::index';
    }

}