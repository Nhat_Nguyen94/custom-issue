<?php
namespace Dayone\Issuer;

class UnileverApp {

    public function __construct(){

    }

    public function view()
    {
        \App::register('Dayone\Issuer\UnileverAppServiceProvider');
        return 'UnileverApp::index';
    }

}