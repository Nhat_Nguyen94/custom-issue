<?php
namespace Dayone\Issuer;

class Unilever_Tresemme_PS {

    public function __construct(){

    }

    public function view()
    {
        \App::register('Dayone\Issuer\Unilever_Tresemme_PSServiceProvider');
        return 'Unilever_Tresemme_PS::index';
    }

}